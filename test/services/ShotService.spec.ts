import { Room } from "../../src/models/haxball/Room";
import { ShotService } from "../../src/services/ShotService";
import { Player } from "../../src/models/haxball/Player";
import { Team } from "../../src/models/haxball/Team";
import { Disc } from "../../src/models/haxball/Disc";
import { StadiumStore } from "../../src/store/StadiumStore";
import { Stadium } from "../../src/models/Stadium";

jest.mock("../../src/store/StadiumStore");
const currentStadiumMock = jest.fn();
StadiumStore.getCurrentStadium = currentStadiumMock;

jest.mock("../../src", () => ({
  __esModule: true,
  room: <any>{ getDiscProperties: jest.fn() },
}));
// @ts-ignore
import { room } from "../../src";
const roomMock: Room = room;
(roomMock.getDiscProperties as jest.Mock).mockReturnValue(buildBallDisc());

describe("ShotService", () => {
  beforeEach(() => {
    currentStadiumMock.mockReset();
  });

  it("should be on target", () => {
    // Arrange
    const shotService = new ShotService();
    const player: Player = buildPlayer(Team.BlueTeam, -10, 0);
    currentStadiumMock.mockReturnValue(buildStadium());

    // Act
    const result = shotService.isShotOnTarget(player);

    // Assert
    expect(result).toBeTruthy();
  });
});

function buildPlayer(team: Team, xPosition: number, yPosition: number): Player {
  return {
    admin: true,
    auth: "",
    conn: "",
    id: 1,
    name: "player1",
    position: { x: xPosition, y: yPosition },
    team: team,
  } as Player;
}

function buildBallDisc(): Disc {
  return { damping: 0.99, x: -10, y: 0, xspeed: -10, yspeed: 0 } as Disc;
}
function buildStadium(): Stadium {
  return {
    goalAreas: {
      red: {
        xCoord: -100,
        yUpperCoord: 50,
        yLowerCoord: -50,
      },
      blue: {
        xCoord: 100,
        yUpperCoord: 50,
        yLowerCoord: -50,
      },
    },
  } as Stadium;
}

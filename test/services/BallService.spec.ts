import { BallService } from "../../src/services/BallService";
import { BallKickedBy } from "../../src/models/Ball";
import { ShotService } from "../../src/services/ShotService";
import { Player } from "../../src/models/haxball/Player";
import { Team } from "../../src/models/haxball/Team";

jest.mock("../../src/services/ShotService", () => ({
  __esModule: true,
  shotService: <any>{ isShotOnTarget: jest.fn() },
}));
import { shotService } from "../../src/services/ShotService";
const shotServiceMock: ShotService = shotService;

describe("BallService", () => {
  describe("Ball kicked", () => {
    it("should set ball kicked", () => {
      // Arrange
      const ballService = new BallService();
      const playerName = "TestName";
      const player: Player = buildPlayer(playerName, Team.BlueTeam, 0, 0);

      const now = Date.now();
      // Act
      ballService.setBallKicked(player);

      // Assert
      const ballKickedBy: BallKickedBy = ballService.getBallKicked();
      expect(ballKickedBy).not.toBeNull();
      expect(ballKickedBy.player).not.toBeNull();
      expect(ballKickedBy.player?.name).toBe(playerName);
      expect(ballKickedBy.time).toBeGreaterThanOrEqual(now);
      expect(ballKickedBy.onTarget).toBeFalsy();
    });
  });
});

function buildPlayer(
  name: string,
  team: Team,
  xPos: number,
  yPos: number
): Player {
  return {
    name: name,
    team: team,
    position: {
      x: xPos,
      y: yPos,
    },
  } as Player;
}

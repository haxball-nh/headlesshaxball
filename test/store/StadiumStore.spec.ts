import { StadiumStore } from "../../src/store/StadiumStore";
import { Stadium, StadiumName } from "../../src/models/Stadium";

describe("StadiumStore", () => {
  describe("Stadium Initialization", () => {
    it("Should initialize the stadium list", () => {
      // Arrange

      // Act
      const futsal1 = StadiumStore.getStadium(
        StadiumName.FUTSAL_1X1
      ) as Stadium;

      // Assert
      expect(futsal1.name).toBe(StadiumName.FUTSAL_1X1);
      expect(futsal1.currentStadium).toBeFalsy();
      expect(futsal1.pitchData).not.toBeNull();
    });
  });
});

const path = require("path");
const webpack = require("webpack");

module.exports = {
  // Change to your "entry-point".
  entry: "./src/index",
  mode: "production",
  output: {
    path: path.resolve(__dirname, "public/"),
    filename: "bot.js",
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".json"],
  },
  module: {
    rules: [
      {
        // Include ts, tsx, js, and jsx files.
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        loader: "babel-loader",
      },
    ],
  },
};

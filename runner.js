const puppeteer = require("puppeteer");
const figlet = require("figlet");

async function bot() {
  const browser = await puppeteer.launch({
    executablePath: "/usr/bin/chromium-browser",
    args: [
      "--disable-web-security",
      "--disable-notifications",
      "--no-sandbox",
      "--disable-setuid-sandbox",
    ],
    defaultViewport: {
      height: 1080,
      width: 1920,
    },
    headless: true,
  });

  const page = await browser.newPage();

  page
    .on("console", (message) =>
      console.log(
        `${message.type().substr(0, 3).toUpperCase()} ${message.text()}`
      )
    )
    .on("pageerror", ({ message }) => console.log(message))
    .on("response", (response) =>
      console.log(`${response.status()} ${response.url()}`)
    )
    .on("requestfailed", (request) =>
      console.log(`${request.failure().errorText} ${request.url()}`)
    );

  await page.goto("https://www.haxball.com/headless", {
    waitUntil: "networkidle2",
  });
  await page.addScriptTag({ path: "config/env-config.js" });
  await page.addScriptTag({ path: "public/bot.js" });

  figlet.text(
    "Haxball Bot\nis running...\n" + process.env["ROOM_NAME"],
    function (err, data) {
      if (err) {
        console.log("Something went wrong...");
        console.dir(err);
        return;
      }
      console.log(data);
    }
  );
}

bot();

module.exports = {
  presets: [
    ["@babel/preset-env", { targets: { node: "current" } }],
    "@babel/preset-typescript",
  ],
  collectCoverage: true,
  collectCoverageFrom: ["src/**/*.{js,ts,vue}", "!**/node_modules/**"],
  coverageReporters: ["text"],
};

#!/bin/sh

cat << EOF
function ROOM_NAME() {
  return "$ROOM_NAME";
}
function PASSWORD() {
  return "$PASSWORD";
}
function SAS_TOKEN() {
  return "$SAS_TOKEN";
}
function TABLE_URL() {
  return "$TABLE_URL";
}
function TOKEN() {
  return "$TOKEN";
}
EOF

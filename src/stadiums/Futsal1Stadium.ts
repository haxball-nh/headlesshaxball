export const futsal1Stadium = `{

\t"name" : "Futsal 1x1 2x2",

\t"width" : 420,

\t"height" : 200,

\t"spawnDistance" : 180,

\t"bg" : { "type" : "hockey", "width" : 368, "height" : 171, "kickOffRadius" : 65, "cornerRadius" : 0 },

\t"vertexes" : [
\t\t/* 0 */ { "x" : -368, "y" : 171, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t/* 1 */ { "x" : -368, "y" : 65, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t/* 2 */ { "x" : -368, "y" : -65, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t/* 3 */ { "x" : -368, "y" : -171, "trait" : "ballArea", "bCoef" : 1, "cMask" : ["ball" ] },
\t\t/* 4 */ { "x" : 368, "y" : 171, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t/* 5 */ { "x" : 368, "y" : 65, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t/* 6 */ { "x" : 368, "y" : -65, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t/* 7 */ { "x" : 368, "y" : -171, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 8 */ { "x" : 0, "y" : 65, "trait" : "kickOffBarrier" },
\t\t
\t\t/* 9 */ { "x" : 0, "y" : -65, "trait" : "line" },
\t\t
\t\t/* 10 */ { "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "x" : -384, "y" : -65 },
\t\t/* 11 */ { "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "x" : 384, "y" : -65 },
\t\t/* 12 */ { "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "x" : -384, "y" : 65 },
\t\t/* 13 */ { "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "x" : 384, "y" : 65 },
\t\t
\t\t/* 14 */ { "bCoef" : 1, "trait" : "ballArea", "x" : 368, "y" : 171 },
\t\t/* 15 */ { "bCoef" : 1, "trait" : "ballArea", "x" : 368, "y" : -171 },
\t\t
\t\t/* 16 */ { "bCoef" : 0, "trait" : "line", "x" : 0, "y" : 171 },
\t\t/* 17 */ { "bCoef" : 0, "trait" : "line", "x" : 0, "y" : -171 },
\t\t
\t\t/* 18 */ { "x" : 0, "y" : 65, "trait" : "kickOffBarrier" },
\t\t/* 19 */ { "x" : 0, "y" : -65, "trait" : "kickOffBarrier" },
\t\t
\t\t/* 20 */ { "x" : 377, "y" : -65, "trait" : "line", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 21 */ { "x" : 377, "y" : -171, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 22 */ { "x" : -377, "y" : -65, "trait" : "line", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 23 */ { "x" : -377, "y" : -171, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 24 */ { "x" : -377, "y" : 65, "trait" : "line", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 25 */ { "x" : -377, "y" : 171, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 26 */ { "x" : 377, "y" : 65, "trait" : "line", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 27 */ { "x" : 377, "y" : 171, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 28 */ { "x" : 0, "y" : 199, "trait" : "kickOffBarrier" },
\t\t/* 29 */ { "x" : 0, "y" : 65, "trait" : "kickOffBarrier" },
\t\t/* 30 */ { "x" : 0, "y" : -65, "trait" : "kickOffBarrier" },
\t\t/* 31 */ { "x" : 0, "y" : -199, "trait" : "kickOffBarrier" },
\t\t
\t\t/* 32 */ { "x" : -368, "y" : 118, "trait" : "redDefenceArea" },
\t\t/* 33 */ { "x" : -250, "y" : 118, "trait" : "redDefenceArea" },
\t\t/* 34 */ { "x" : -250, "y" : -118, "trait" : "redDefenceArea" },
\t\t/* 35 */ { "x" : -368, "y" : -118, "trait" : "redDefenceArea" },
\t\t
\t\t/* 36 */ { "x" : 250, "y" : 118, "trait" : "blueDefenceArea" },
\t\t/* 37 */ { "x" : 368, "y" : 118, "trait" : "blueDefenceArea" },
\t\t/* 38 */ { "x" : 368, "y" : -118, "trait" : "blueDefenceArea" },
\t\t/* 39 */ { "x" : 250, "y" : -118, "trait" : "blueDefenceArea" }

\t],

\t"segments" : [
\t\t{ "v0" : 0, "v1" : 1, "trait" : "ballArea" },
\t\t{ "v0" : 2, "v1" : 3, "trait" : "ballArea" },
\t\t{ "v0" : 4, "v1" : 5, "trait" : "ballArea" },
\t\t{ "v0" : 6, "v1" : 7, "trait" : "ballArea" },
\t\t
\t\t{ "v0" : 8, "v1" : 9, "trait" : "kickOffBarrier", "curve" : 180, "cGroup" : ["blueKO" ] },
\t\t{ "v0" : 8, "v1" : 9, "trait" : "kickOffBarrier", "curve" : -180, "cGroup" : ["redKO" ] },
\t\t
\t\t{ "vis" : true, "bCoef" : 0.1, "cMask" : ["all" ], "trait" : "goalNet", "v0" : 2, "v1" : 10, "color" : "FFFFFF", "curve" : -35 },
\t\t{ "vis" : true, "bCoef" : 0.1, "cMask" : ["all" ], "trait" : "goalNet", "v0" : 6, "v1" : 11, "color" : "FFFFFF", "curve" : 35 },
\t\t{ "vis" : true, "bCoef" : 0.1, "cMask" : ["all" ], "trait" : "goalNet", "v0" : 1, "v1" : 12, "color" : "FFFFFF", "curve" : 35 },
\t\t{ "vis" : true, "bCoef" : 0.1, "cMask" : ["all" ], "trait" : "goalNet", "v0" : 5, "v1" : 13, "color" : "FFFFFF", "curve" : -35 },
\t\t{ "vis" : true, "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "v0" : 10, "v1" : 12, "x" : -585, "color" : "FFFFFF", "curve" : -35 },
\t\t{ "vis" : true, "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "v0" : 11, "v1" : 13, "x" : 585, "color" : "FFFFFF", "curve" : 35 },
\t\t
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 1, "v1" : 0, "cMask" : ["ball" ], "x" : -368 },
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 5, "v1" : 4, "cMask" : ["ball" ], "x" : 368 },
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 2, "v1" : 3, "cMask" : ["ball" ], "x" : -368 },
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 6, "v1" : 7, "cMask" : ["ball" ], "x" : 368 },
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 0, "v1" : 14, "y" : 171 },
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 3, "v1" : 15, "y" : -171 },
\t\t
\t\t{ "curve" : 0, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line", "v0" : 16, "v1" : 17 },
\t\t{ "curve" : -180, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line", "v0" : 9, "v1" : 8 },
\t\t{ "curve" : 180, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line", "v0" : 19, "v1" : 18 },
\t\t{ "curve" : 0, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line", "v0" : 2, "v1" : 1 },
\t\t{ "curve" : 0, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line", "v0" : 6, "v1" : 5 },
\t\t
\t\t{ "vis" : false, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 20, "v1" : 21, "cMask" : ["ball" ], "x" : 330 },
\t\t{ "vis" : false, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 22, "v1" : 23, "cMask" : ["ball" ], "x" : -330 },
\t\t{ "vis" : false, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 24, "v1" : 25, "cMask" : ["ball" ], "x" : -330 },
\t\t{ "vis" : false, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 26, "v1" : 27, "cMask" : ["ball" ], "x" : 330 },
\t\t
\t\t{ "v0" : 28, "v1" : 29, "trait" : "kickOffBarrier" },
\t\t{ "v0" : 30, "v1" : 31, "trait" : "kickOffBarrier" },
\t\t
\t\t{ "v0" : 32, "v1" : 33, "trait" : "redDefenceArea", "bias" : -1 },
\t\t{ "v0" : 33, "v1" : 34, "trait" : "redDefenceArea", "bias" : -1 },
\t\t{ "v0" : 34, "v1" : 35, "trait" : "redDefenceArea", "bias" : -1 },
\t\t{ "v0" : 32, "v1" : 35, "trait" : "redDefenceArea", "bias" : 1 },
\t\t
\t\t{ "v0" : 36, "v1" : 37, "trait" : "blueDefenceArea", "bias" : -1 },
\t\t{ "v0" : 37, "v1" : 38, "trait" : "blueDefenceArea", "bias" : -1 },
\t\t{ "v0" : 38, "v1" : 39, "trait" : "blueDefenceArea", "bias" : -1 },
\t\t{ "v0" : 36, "v1" : 39, "trait" : "blueDefenceArea", "bias" : 1 }

\t],

\t"goals" : [
\t\t{ "p0" : [-377,-65 ], "p1" : [-377,65 ], "team" : "red" },
\t\t{ "p0" : [377,65 ], "p1" : [377,-65 ], "team" : "blue" }

\t],

\t"discs" : [
\t\t{ "pos" : [-368,65 ], "trait" : "goalPost", "color" : "FFFFFF", "radius" : 5 },
\t\t{ "pos" : [-368,-65 ], "trait" : "goalPost", "color" : "FFFFFF", "radius" : 5 },
\t\t{ "pos" : [368,65 ], "trait" : "goalPost", "color" : "FFFFFF", "radius" : 5 },
\t\t{ "pos" : [368,-65 ], "trait" : "goalPost", "color" : "FFFFFF", "radius" : 5 }

\t],

\t"planes" : [
\t\t{ "normal" : [0,1 ], "dist" : -171, "trait" : "ballArea" },
\t\t{ "normal" : [0,-1 ], "dist" : -171, "trait" : "ballArea" },
\t\t
\t\t{ "normal" : [0,1 ], "dist" : -200, "bCoef" : 0.2, "cMask" : ["all" ] },
\t\t{ "normal" : [0,-1 ], "dist" : -200, "bCoef" : 0.2, "cMask" : ["all" ] },
\t\t{ "normal" : [1,0 ], "dist" : -420, "bCoef" : 0.2, "cMask" : ["all" ] },
\t\t{ "normal" : [-1,0 ], "dist" : -420, "bCoef" : 0.2, "cMask" : ["all" ] }

\t],

\t"traits" : {
\t\t"ballArea" : { "vis" : false, "bCoef" : 1, "cMask" : ["ball" ] },
\t\t"goalPost" : { "radius" : 8, "invMass" : 0, "bCoef" : 1 },
\t\t"goalNet" : { "vis" : true, "bCoef" : 0.1, "cMask" : ["all" ] },
\t\t"kickOffBarrier" : { "vis" : false, "bCoef" : 0.1, "cGroup" : ["redKO","blueKO" ], "cMask" : ["red","blue" ] },
\t\t"line" : { "vis" : true, "bCoef" : 0, "cMask" : ["" ] },
\t\t"arco" : { "radius" : 2, "cMask" : ["n/d" ], "color" : "cccccc" },
\t\t"redDefenceArea" : { "vis" : true, "color" : "FFFFFF", "bCoef" : 0.1, "cGroup" : ["red" ], "cMask" : ["c0" ] },
\t\t"blueDefenceArea" : { "vis" : true, "color" : "FFFFFF", "bCoef" : 0.1, "cGroup" : ["blue" ], "cMask" : ["c1" ] }

\t},

\t"playerPhysics" : {
\t\t"acceleration" : 0.11,
\t\t"kickingAcceleration" : 0.1,
\t\t"kickStrength" : 7

\t},

\t"ballPhysics" : {
\t\t"radius" : 6.4,
\t\t"color" : "EAFF00"

\t}
}`;

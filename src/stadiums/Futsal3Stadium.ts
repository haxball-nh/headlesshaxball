export const futsal3Stadium = `{

\t"name" : "Futsal 3x3 4x4",

\t"width" : 755,

\t"height" : 339,

\t"spawnDistance" : 310,

\t"bg" : { "type" : "hockey", "width" : 665, "height" : 290, "kickOffRadius" : 80, "cornerRadius" : 0 },

\t"vertexes" : [
\t\t/* 0 */ { "x" : -665, "y" : 290, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t/* 1 */ { "x" : -665, "y" : 80, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t/* 2 */ { "x" : -665, "y" : -80, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t/* 3 */ { "x" : -665, "y" : -290, "trait" : "ballArea", "bCoef" : 1, "cMask" : ["ball" ] },
\t\t/* 4 */ { "x" : 665, "y" : 290, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t/* 5 */ { "x" : 665, "y" : 80, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t/* 6 */ { "x" : 665, "y" : -80, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t/* 7 */ { "x" : 665, "y" : -290, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 8 */ { "x" : 0, "y" : 306, "trait" : "kickOffBarrier" },
\t\t/* 9 */ { "x" : 0, "y" : 80, "trait" : "kickOffBarrier" },
\t\t
\t\t/* 10 */ { "x" : 0, "y" : -80, "trait" : "line" },
\t\t
\t\t/* 11 */ { "x" : 0, "y" : -306, "trait" : "kickOffBarrier" },
\t\t
\t\t/* 12 */ { "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "x" : -693, "y" : -80 },
\t\t/* 13 */ { "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "x" : 693, "y" : -80 },
\t\t/* 14 */ { "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "x" : -693, "y" : 80 },
\t\t/* 15 */ { "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "x" : 693, "y" : 80 },
\t\t
\t\t/* 16 */ { "trait" : "line", "x" : -665, "y" : -215 },
\t\t/* 17 */ { "trait" : "line", "x" : -500, "y" : -50 },
\t\t/* 18 */ { "trait" : "line", "x" : 665, "y" : -215 },
\t\t/* 19 */ { "trait" : "line", "x" : 500, "y" : -50 },
\t\t/* 20 */ { "trait" : "line", "x" : -665, "y" : 215 },
\t\t/* 21 */ { "trait" : "line", "x" : -500, "y" : 50 },
\t\t/* 22 */ { "trait" : "line", "x" : 665, "y" : 215 },
\t\t/* 23 */ { "trait" : "line", "x" : 500, "y" : 50 },
\t\t
\t\t/* 24 */ { "bCoef" : 1, "trait" : "ballArea", "x" : 665, "y" : 290 },
\t\t/* 25 */ { "bCoef" : 1, "trait" : "ballArea", "x" : 665, "y" : -290 },
\t\t
\t\t/* 26 */ { "bCoef" : 0, "trait" : "line", "x" : 0, "y" : 290 },
\t\t/* 27 */ { "bCoef" : 0, "trait" : "line", "x" : 0, "y" : -290 },
\t\t
\t\t/* 28 */ { "x" : 0, "y" : 80, "trait" : "kickOffBarrier" },
\t\t/* 29 */ { "x" : 0, "y" : -80, "trait" : "kickOffBarrier" },
\t\t
\t\t/* 30 */ { "x" : 674, "y" : -80, "trait" : "line", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 31 */ { "x" : 674, "y" : -290, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 32 */ { "x" : -674, "y" : -80, "trait" : "line", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 33 */ { "x" : -674, "y" : -290, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 34 */ { "x" : -674, "y" : 80, "trait" : "line", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 35 */ { "x" : -674, "y" : 290, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 36 */ { "x" : 674, "y" : 80, "trait" : "line", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 37 */ { "x" : 674, "y" : 290, "trait" : "ballArea", "cMask" : ["ball" ], "bCoef" : 1 },
\t\t
\t\t/* 38 */ { "x" : -665, "y" : 185, "trait" : "redDefenceArea" },
\t\t/* 39 */ { "x" : -500, "y" : 185, "trait" : "redDefenceArea" },
\t\t/* 40 */ { "x" : -500, "y" : -185, "trait" : "redDefenceArea" },
\t\t/* 41 */ { "x" : -665, "y" : -185, "trait" : "redDefenceArea" },
\t\t
\t\t/* 42 */ { "x" : 500, "y" : 185, "trait" : "blueDefenceArea" },
\t\t/* 43 */ { "x" : 665, "y" : 185, "trait" : "blueDefenceArea", "_selected" : "segment" },
\t\t/* 44 */ { "x" : 665, "y" : -185, "trait" : "blueDefenceArea", "_selected" : "segment" },
\t\t/* 45 */ { "x" : 500, "y" : -185, "trait" : "blueDefenceArea" }

\t],

\t"segments" : [
\t\t{ "v0" : 0, "v1" : 1, "trait" : "ballArea" },
\t\t{ "v0" : 2, "v1" : 3, "trait" : "ballArea" },
\t\t{ "v0" : 4, "v1" : 5, "trait" : "ballArea" },
\t\t{ "v0" : 6, "v1" : 7, "trait" : "ballArea" },
\t\t
\t\t{ "v0" : 8, "v1" : 9, "trait" : "kickOffBarrier" },
\t\t{ "v0" : 9, "v1" : 10, "trait" : "kickOffBarrier", "curve" : 180, "cGroup" : ["blueKO" ] },
\t\t{ "v0" : 9, "v1" : 10, "trait" : "kickOffBarrier", "curve" : -180, "cGroup" : ["redKO" ] },
\t\t{ "v0" : 10, "v1" : 11, "trait" : "kickOffBarrier" },
\t\t
\t\t{ "vis" : true, "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "v0" : 2, "v1" : 12, "color" : "FFFFFF", "curve" : -35 },
\t\t{ "vis" : true, "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "v0" : 6, "v1" : 13, "color" : "FFFFFF", "curve" : 35 },
\t\t{ "vis" : true, "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "v0" : 1, "v1" : 14, "color" : "FFFFFF", "curve" : 35 },
\t\t{ "vis" : true, "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "v0" : 5, "v1" : 15, "color" : "FFFFFF", "curve" : -35 },
\t\t{ "vis" : true, "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "v0" : 12, "v1" : 14, "x" : -585, "color" : "FFFFFF", "curve" : -35 },
\t\t{ "vis" : true, "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "v0" : 13, "v1" : 15, "x" : 585, "color" : "FFFFFF", "curve" : 35 },
\t\t
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line", "v0" : 17, "v1" : 21, "curve" : 0 },
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line", "v0" : 19, "v1" : 23, "curve" : 0 },
\t\t
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 1, "v1" : 0, "cMask" : ["ball" ], "x" : -665 },
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 5, "v1" : 4, "cMask" : ["ball" ], "x" : 665 },
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 2, "v1" : 3, "cMask" : ["ball" ], "x" : -665 },
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 6, "v1" : 7, "cMask" : ["ball" ], "x" : 665 },
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 0, "v1" : 24, "y" : 290 },
\t\t{ "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 3, "v1" : 25, "y" : -290 },
\t\t
\t\t{ "curve" : 0, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line", "v0" : 26, "v1" : 27 },
\t\t{ "curve" : -180, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line", "v0" : 10, "v1" : 9 },
\t\t{ "curve" : 180, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line", "v0" : 29, "v1" : 28 },
\t\t{ "curve" : 0, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line", "v0" : 2, "v1" : 1 },
\t\t{ "curve" : 0, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line", "v0" : 6, "v1" : 5 },
\t\t
\t\t{ "vis" : false, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 30, "v1" : 31, "cMask" : ["ball" ], "x" : 614 },
\t\t{ "vis" : false, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 32, "v1" : 33, "cMask" : ["ball" ], "x" : -614 },
\t\t{ "vis" : false, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 34, "v1" : 35, "cMask" : ["ball" ], "x" : -614 },
\t\t{ "vis" : false, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "v0" : 36, "v1" : 37, "cMask" : ["ball" ], "x" : 614 },
\t\t
\t\t{ "v0" : 38, "v1" : 39, "trait" : "redDefenceArea", "bias" : -1 },
\t\t{ "v0" : 39, "v1" : 40, "trait" : "redDefenceArea", "bias" : -1 },
\t\t{ "v0" : 40, "v1" : 41, "trait" : "redDefenceArea", "bias" : -1 },
\t\t{ "v0" : 38, "v1" : 41, "trait" : "redDefenceArea", "bias" : 1 },
\t\t
\t\t{ "v0" : 42, "v1" : 43, "trait" : "blueDefenceArea", "bias" : -1 },
\t\t{ "v0" : 43, "v1" : 44, "trait" : "blueDefenceArea", "bias" : -1 },
\t\t{ "v0" : 44, "v1" : 45, "trait" : "blueDefenceArea", "bias" : -1 },
\t\t{ "v0" : 42, "v1" : 45, "trait" : "blueDefenceArea", "bias" : 1 }

\t],

\t"goals" : [
\t\t{ "p0" : [-674,-80 ], "p1" : [-674,80 ], "team" : "red" },
\t\t{ "p0" : [674,80 ], "p1" : [674,-80 ], "team" : "blue" }

\t],

\t"discs" : [
\t\t{ "pos" : [-665,80 ], "trait" : "goalPost", "color" : "FFFFFF", "radius" : 5 },
\t\t{ "pos" : [-665,-80 ], "trait" : "goalPost", "color" : "FFFFFF", "radius" : 5 },
\t\t{ "pos" : [665,80 ], "trait" : "goalPost", "color" : "FFFFFF", "radius" : 5 },
\t\t{ "pos" : [665,-80 ], "trait" : "goalPost", "color" : "FFFFFF", "radius" : 5 }

\t],

\t"planes" : [
\t\t{ "normal" : [0,1 ], "dist" : -290, "trait" : "ballArea" },
\t\t{ "normal" : [0,-1 ], "dist" : -290, "trait" : "ballArea" },
\t\t
\t\t{ "normal" : [0,1 ], "dist" : -339, "bCoef" : 0.2, "cMask" : ["all" ] },
\t\t{ "normal" : [0,-1 ], "dist" : -339, "bCoef" : 0.2, "cMask" : ["all" ] },
\t\t{ "normal" : [1,0 ], "dist" : -755, "bCoef" : 0.2, "cMask" : ["all" ] },
\t\t{ "normal" : [-1,0 ], "dist" : -755, "bCoef" : 0.2, "cMask" : ["all" ] }

\t],

\t"traits" : {
\t\t"ballArea" : { "vis" : false, "bCoef" : 1, "cMask" : ["ball" ] },
\t\t"goalPost" : { "radius" : 8, "invMass" : 0, "bCoef" : 1 },
\t\t"goalNet" : { "vis" : true, "bCoef" : 0.1, "cMask" : ["all" ] },
\t\t"kickOffBarrier" : { "vis" : false, "bCoef" : 0.1, "cGroup" : ["redKO","blueKO" ], "cMask" : ["red","blue" ] },
\t\t"line" : { "vis" : true, "bCoef" : 0, "cMask" : ["" ] },
\t\t"arco" : { "radius" : 2, "cMask" : ["n/d" ], "color" : "cccccc" },
\t\t"redDefenceArea" : { "vis" : true, "color" : "FFFFFF", "bCoef" : 0.1, "cGroup" : ["red" ], "cMask" : ["c0" ] },
\t\t"blueDefenceArea" : { "vis" : true, "color" : "FFFFFF", "bCoef" : 0.1, "cGroup" : ["blue" ], "cMask" : ["c1" ] }

\t},

\t"playerPhysics" : {
\t\t"acceleration" : 0.11,
\t\t"kickingAcceleration" : 0.1,
\t\t"kickStrength" : 7

\t},

\t"ballPhysics" : {
\t\t"radius" : 6.4,
\t\t"color" : "EAFF00"

\t}
}`;

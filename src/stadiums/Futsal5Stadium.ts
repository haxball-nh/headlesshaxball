export const futsal5Stadium = `{

\t"name" : "Futsal 5x5 6x6",

\t"width" : 900,

\t"height" : 404,

\t"spawnDistance" : 310,

\t"bg" : { "type" : "hockey", "width" : 793, "height" : 346, "kickOffRadius" : 95, "cornerRadius" : 0 },

\t"vertexes" : [
\t\t/* 0 */ { "x" : -793, "y" : 346, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea" },
\t\t/* 1 */ { "x" : -793, "y" : 95, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea" },
\t\t/* 2 */ { "x" : -793, "y" : -95, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea" },
\t\t/* 3 */ { "x" : -793, "y" : -346, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea" },
\t\t/* 4 */ { "x" : 793, "y" : 346, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea" },
\t\t/* 5 */ { "x" : 793, "y" : 95, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea" },
\t\t/* 6 */ { "x" : 793, "y" : -95, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea" },
\t\t/* 7 */ { "x" : 793, "y" : -346, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea" },
\t\t
\t\t/* 8 */ { "x" : 0, "y" : 404, "trait" : "kickOffBarrier" },
\t\t/* 9 */ { "x" : 0, "y" : 95, "trait" : "kickOffBarrier" },
\t\t
\t\t/* 10 */ { "x" : 0, "y" : -95, "trait" : "line" },
\t\t
\t\t/* 11 */ { "x" : 0, "y" : -404, "trait" : "kickOffBarrier" },
\t\t
\t\t/* 12 */ { "x" : -821, "y" : -95, "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet" },
\t\t/* 13 */ { "x" : 821, "y" : -95, "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet" },
\t\t/* 14 */ { "x" : -821, "y" : 95, "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet" },
\t\t/* 15 */ { "x" : 821, "y" : 95, "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet" },
\t\t
\t\t/* 16 */ { "x" : -793, "y" : -271, "trait" : "line" },
\t\t/* 17 */ { "x" : -572, "y" : -50, "trait" : "line" },
\t\t/* 18 */ { "x" : 793, "y" : -271, "trait" : "line" },
\t\t/* 19 */ { "x" : 572, "y" : -50, "trait" : "line" },
\t\t/* 20 */ { "x" : -793, "y" : 271, "trait" : "line" },
\t\t/* 21 */ { "x" : -572, "y" : 50, "trait" : "line" },
\t\t/* 22 */ { "x" : 793, "y" : 271, "trait" : "line" },
\t\t/* 23 */ { "x" : 572, "y" : 50, "trait" : "line" },
\t\t
\t\t/* 24 */ { "x" : 793, "y" : 346, "bCoef" : 1, "trait" : "ballArea" },
\t\t/* 25 */ { "x" : 793, "y" : -346, "bCoef" : 1, "trait" : "ballArea" },
\t\t
\t\t/* 26 */ { "x" : 0, "y" : 346, "bCoef" : 0, "trait" : "line" },
\t\t/* 27 */ { "x" : 0, "y" : -346, "bCoef" : 0, "trait" : "line" },
\t\t
\t\t/* 28 */ { "x" : 0, "y" : 95, "trait" : "kickOffBarrier" },
\t\t/* 29 */ { "x" : 0, "y" : -95, "trait" : "kickOffBarrier" },
\t\t
\t\t/* 30 */ { "x" : 802, "y" : -98, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "line" },
\t\t
\t\t/* 31 */ { "x" : 802, "y" : -346, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea" },
\t\t
\t\t/* 32 */ { "x" : -802, "y" : -98, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "line" },
\t\t
\t\t/* 33 */ { "x" : -802, "y" : -346, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea" },
\t\t
\t\t/* 34 */ { "x" : -802, "y" : 98, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "line" },
\t\t
\t\t/* 35 */ { "x" : -802, "y" : 346, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea" },
\t\t
\t\t/* 36 */ { "x" : 802, "y" : 98, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "line" },
\t\t
\t\t/* 37 */ { "x" : 802, "y" : 346, "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea" },
\t\t
\t\t/* 38 */ { "x" : -793, "y" : 220, "trait" : "redDefenceArea" },
\t\t/* 39 */ { "x" : -572, "y" : 220, "trait" : "redDefenceArea" },
\t\t/* 40 */ { "x" : -572, "y" : -220, "trait" : "redDefenceArea" },
\t\t/* 41 */ { "x" : -793, "y" : -220, "trait" : "redDefenceArea" },
\t\t
\t\t/* 42 */ { "x" : 572, "y" : 220, "trait" : "blueDefenceArea" },
\t\t/* 43 */ { "x" : 793, "y" : 220, "trait" : "blueDefenceArea" },
\t\t/* 44 */ { "x" : 793, "y" : -220, "trait" : "blueDefenceArea" },
\t\t/* 45 */ { "x" : 572, "y" : -220, "trait" : "blueDefenceArea" }

\t],

\t"segments" : [
\t\t{ "v0" : 0, "v1" : 1, "trait" : "ballArea" },
\t\t{ "v0" : 2, "v1" : 3, "trait" : "ballArea" },
\t\t{ "v0" : 4, "v1" : 5, "trait" : "ballArea" },
\t\t{ "v0" : 6, "v1" : 7, "trait" : "ballArea" },
\t\t
\t\t{ "v0" : 8, "v1" : 9, "trait" : "kickOffBarrier" },
\t\t{ "v0" : 9, "v1" : 10, "curve" : 180, "cGroup" : ["blueKO" ], "trait" : "kickOffBarrier" },
\t\t{ "v0" : 9, "v1" : 10, "curve" : -180, "cGroup" : ["redKO" ], "trait" : "kickOffBarrier" },
\t\t{ "v0" : 10, "v1" : 11, "trait" : "kickOffBarrier" },
\t\t
\t\t{ "v0" : 2, "v1" : 12, "curve" : -35, "vis" : true, "color" : "FFFFFF", "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "y" : -95 },
\t\t{ "v0" : 6, "v1" : 13, "curve" : 35, "vis" : true, "color" : "FFFFFF", "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "y" : -95 },
\t\t{ "v0" : 1, "v1" : 14, "curve" : 35, "vis" : true, "color" : "FFFFFF", "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "y" : 95 },
\t\t{ "v0" : 5, "v1" : 15, "curve" : -35, "vis" : true, "color" : "FFFFFF", "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "y" : 95 },
\t\t{ "v0" : 12, "v1" : 14, "curve" : -35, "vis" : true, "color" : "FFFFFF", "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "x" : -821 },
\t\t{ "v0" : 13, "v1" : 15, "curve" : 35, "vis" : true, "color" : "FFFFFF", "bCoef" : 0.1, "cMask" : ["ball" ], "trait" : "goalNet", "x" : 585 },
\t\t
\t\t{ "v0" : 1, "v1" : 0, "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea", "x" : -665 },
\t\t{ "v0" : 5, "v1" : 4, "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea", "x" : 665 },
\t\t{ "v0" : 2, "v1" : 3, "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea", "x" : -665 },
\t\t{ "v0" : 6, "v1" : 7, "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea", "x" : 665 },
\t\t{ "v0" : 0, "v1" : 24, "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "y" : 290 },
\t\t{ "v0" : 3, "v1" : 25, "vis" : true, "color" : "FFFFFF", "bCoef" : 1, "trait" : "ballArea", "y" : -290 },
\t\t
\t\t{ "v0" : 26, "v1" : 27, "curve" : 0, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line" },
\t\t{ "v0" : 10, "v1" : 9, "curve" : -180, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line" },
\t\t{ "v0" : 29, "v1" : 28, "curve" : 180, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line" },
\t\t{ "v0" : 2, "v1" : 1, "curve" : 0, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line" },
\t\t{ "v0" : 6, "v1" : 5, "curve" : 0, "vis" : true, "color" : "FFFFFF", "bCoef" : 0, "trait" : "line" },
\t\t
\t\t{ "v0" : 30, "v1" : 31, "vis" : false, "color" : "FFFFFF", "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea", "x" : 614 },
\t\t{ "v0" : 32, "v1" : 33, "vis" : false, "color" : "FFFFFF", "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea", "x" : -614 },
\t\t{ "v0" : 34, "v1" : 35, "vis" : false, "color" : "FFFFFF", "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea", "x" : -614 },
\t\t{ "v0" : 36, "v1" : 37, "vis" : false, "color" : "FFFFFF", "bCoef" : 1, "cMask" : ["ball" ], "trait" : "ballArea", "x" : 614 },
\t\t
\t\t{ "v0" : 38, "v1" : 39, "trait" : "redDefenceArea", "bias" : -1 },
\t\t{ "v0" : 39, "v1" : 40, "trait" : "redDefenceArea", "bias" : -1 },
\t\t{ "v0" : 40, "v1" : 41, "trait" : "redDefenceArea", "bias" : -1 },
\t\t{ "v0" : 38, "v1" : 41, "trait" : "redDefenceArea", "bias" : 1 },
\t\t
\t\t{ "v0" : 42, "v1" : 43, "trait" : "blueDefenceArea", "bias" : -1 },
\t\t{ "v0" : 43, "v1" : 44, "trait" : "blueDefenceArea", "bias" : -1 },
\t\t{ "v0" : 44, "v1" : 45, "trait" : "blueDefenceArea", "bias" : -1 },
\t\t{ "v0" : 42, "v1" : 45, "trait" : "blueDefenceArea", "bias" : 1 }

\t],

\t"goals" : [
\t\t{ "p0" : [-802,-95 ], "p1" : [-802,95 ], "team" : "red" },
\t\t{ "p0" : [802,95 ], "p1" : [802,-95 ], "team" : "blue" }

\t],

\t"discs" : [
\t\t{ "radius" : 5, "pos" : [-793,95 ], "color" : "FFFFFF", "trait" : "goalPost" },
\t\t{ "radius" : 5, "pos" : [-793,-95 ], "color" : "FFFFFF", "trait" : "goalPost" },
\t\t{ "radius" : 5, "pos" : [793,95 ], "color" : "FFFFFF", "trait" : "goalPost" },
\t\t{ "radius" : 5, "pos" : [793,-95 ], "color" : "FFFFFF", "trait" : "goalPost" }

\t],

\t"planes" : [
\t\t{ "normal" : [0,1 ], "dist" : -346, "trait" : "ballArea" },
\t\t{ "normal" : [0,-1 ], "dist" : -346, "trait" : "ballArea" },
\t\t
\t\t{ "normal" : [0,1 ], "dist" : -404, "bCoef" : 0.2, "cMask" : ["all" ] },
\t\t{ "normal" : [0,-1 ], "dist" : -404, "bCoef" : 0.2, "cMask" : ["all" ] },
\t\t{ "normal" : [1,0 ], "dist" : -900, "bCoef" : 0.2, "cMask" : ["all" ] },
\t\t{ "normal" : [-1,0 ], "dist" : -900, "bCoef" : 0.2, "cMask" : ["all" ] }

\t],

\t"traits" : {
\t\t"ballArea" : { "vis" : false, "bCoef" : 1, "cMask" : ["ball" ] },
\t\t"goalPost" : { "radius" : 8, "invMass" : 0, "bCoef" : 1 },
\t\t"goalNet" : { "vis" : true, "bCoef" : 0.1, "cMask" : ["all" ] },
\t\t"kickOffBarrier" : { "vis" : false, "bCoef" : 0.1, "cGroup" : ["redKO","blueKO" ], "cMask" : ["red","blue" ] },
\t\t"line" : { "vis" : true, "bCoef" : 0, "cMask" : ["" ] },
\t\t"arco" : { "radius" : 2, "cMask" : ["n/d" ], "color" : "cccccc" },
\t\t"redDefenceArea" : { "vis" : true, "color" : "FFFFFF", "bCoef" : 0.1, "cGroup" : ["red" ], "cMask" : ["c0" ] },
\t\t"blueDefenceArea" : { "vis" : true, "color" : "FFFFFF", "bCoef" : 0.1, "cGroup" : ["blue" ], "cMask" : ["c1" ] }

\t},

\t"playerPhysics" : {
\t\t"acceleration" : 0.11,
\t\t"kickingAcceleration" : 0.1,
\t\t"kickStrength" : 7

\t},

\t"ballPhysics" : {
\t\t"radius" : 6.4,
\t\t"color" : "EAFF00"

\t}
}`;

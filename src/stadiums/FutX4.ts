export const futX4Stadium = `{

    "name" : "Fut x4",

    "width" : 825,

    "height" : 320,

    "bg" : { "width" : 550, "height" : 240, "kickOffRadius" : 75, "cornerRadius" : 50, "color" : "3D3542" },

    "vertexes" : [
        /* 0 */ { "x" : -551, "y" : -240, "cMask" : ["wall" ] },
        /* 1 */ { "x" : 551, "y" : -240, "cMask" : ["wall" ] },
        /* 2 */ { "x" : -551, "y" : 240, "cMask" : ["wall" ] },
        /* 3 */ { "x" : 551, "y" : 240, "cMask" : ["wall" ] },
        /* 4 */ { "x" : 550, "y" : -241, "cMask" : ["wall" ] },
        /* 5 */ { "x" : 550, "y" : 241, "cMask" : ["wall" ] },
        /* 6 */ { "x" : -550, "y" : -241, "cMask" : ["wall" ] },
        /* 7 */ { "x" : -550, "y" : 241, "cMask" : ["wall" ] },
        /* 8 */ { "x" : 300, "y" : -239, "cMask" : ["wall" ] },
        /* 9 */ { "x" : 300, "y" : 239, "cMask" : ["wall" ] },
        /* 10 */ { "x" : -300, "y" : -239, "cMask" : ["wall" ] },
        /* 11 */ { "x" : -300, "y" : 239, "cMask" : ["wall" ] },
        /* 12 */ { "x" : -554, "y" : -243, "cMask" : ["wall" ] },
        /* 13 */ { "x" : 554, "y" : -243, "cMask" : ["wall" ] },
        /* 14 */ { "x" : -554, "y" : 243, "cMask" : ["wall" ] },
        /* 15 */ { "x" : 554, "y" : 243, "cMask" : ["wall" ] },
        /* 16 */ { "x" : 553, "y" : -244, "cMask" : ["wall" ] },
        /* 17 */ { "x" : 553, "y" : 244, "cMask" : ["wall" ] },
        /* 18 */ { "x" : -553, "y" : -244, "cMask" : ["wall" ] },
        /* 19 */ { "x" : -553, "y" : 244, "cMask" : ["wall" ] },
        /* 20 */ { "x" : 0, "y" : -80, "cMask" : ["red","blue" ], "cGroup" : ["redKO" ] },
        /* 21 */ { "x" : 0, "y" : -580, "cMask" : ["red","blue" ], "cGroup" : ["redKO","blueKO" ] },
        /* 22 */ { "x" : 0, "y" : 80, "cMask" : ["red","blue" ], "cGroup" : ["redKO" ] },
        /* 23 */ { "x" : 0, "y" : 580, "cMask" : ["red","blue" ], "cGroup" : ["redKO","blueKO" ] },
        /* 24 */ { "x" : 0, "y" : -80, "cMask" : ["red","blue" ], "cGroup" : ["blueKO" ] },
        /* 25 */ { "x" : 0, "y" : 80, "cMask" : ["red","blue" ], "cGroup" : ["blueKO" ] },
        /* 26 */ { "x" : -550, "y" : -240, "cMask" : ["ball" ] },
        /* 27 */ { "x" : -550, "y" : -80, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 28 */ { "x" : 550, "y" : -240, "cMask" : ["wall" ] },
        /* 29 */ { "x" : 550, "y" : -80, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 30 */ { "x" : -550, "y" : 80, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 31 */ { "x" : -550, "y" : 240, "cMask" : ["ball" ] },
        /* 32 */ { "x" : 550, "y" : 80, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 33 */ { "x" : 550, "y" : 240, "cMask" : ["wall" ] },
        /* 34 */ { "x" : -586, "y" : -80, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 35 */ { "x" : -550, "y" : -80, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 36 */ { "x" : -586, "y" : 80, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 37 */ { "x" : -550, "y" : 80, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 38 */ { "x" : 550, "y" : -80, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 39 */ { "x" : 586, "y" : -80, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 40 */ { "x" : -585, "y" : -81, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 41 */ { "x" : -585, "y" : 81, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 42 */ { "x" : 585, "y" : -81, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 43 */ { "x" : 585, "y" : 81, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 44 */ { "x" : 550, "y" : 80, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 45 */ { "x" : 586, "y" : 80, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 46 */ { "x" : 0, "y" : -81.5, "cMask" : ["wall" ] },
        /* 47 */ { "x" : 0, "y" : -239, "cMask" : ["wall" ] },
        /* 48 */ { "x" : 0, "y" : 239, "cMask" : ["wall" ] },
        /* 49 */ { "x" : 0, "y" : 81.5, "cMask" : ["wall" ] },
        /* 50 */ { "x" : -300, "y" : -80, "cMask" : ["wall" ] },
        /* 51 */ { "x" : -300, "y" : 80, "cMask" : ["wall" ] },
        /* 52 */ { "x" : 300, "y" : 80, "cMask" : ["wall" ] },
        /* 53 */ { "x" : 300, "y" : -80, "cMask" : ["wall" ] },
        /* 54 */ { "x" : -548.5, "y" : -160, "cMask" : ["wall" ] },
        /* 55 */ { "x" : -370, "y" : -160, "cMask" : ["wall" ] },
        /* 56 */ { "x" : -548.5, "y" : 160, "cMask" : ["wall" ] },
        /* 57 */ { "x" : -370, "y" : 160, "cMask" : ["wall" ] },
        /* 58 */ { "x" : -371, "y" : -161, "cMask" : ["wall" ] },
        /* 59 */ { "x" : -371, "y" : 161, "cMask" : ["wall" ] },
        /* 60 */ { "x" : 371, "y" : -161, "cMask" : ["wall" ] },
        /* 61 */ { "x" : 371, "y" : 161, "cMask" : ["wall" ] },
        /* 62 */ { "x" : 370, "y" : 160, "cMask" : ["wall" ] },
        /* 63 */ { "x" : 548.5, "y" : 160, "cMask" : ["wall" ] },
        /* 64 */ { "x" : 370, "y" : -160, "cMask" : ["wall" ] },
        /* 65 */ { "x" : 548.5, "y" : -160, "cMask" : ["wall" ] },
        /* 66 */ { "x" : -405, "y" : -2.5, "cMask" : ["wall" ] },
        /* 67 */ { "x" : -405, "y" : 2.5, "cMask" : ["wall" ] },
        /* 68 */ { "x" : 0, "y" : -2.5, "cMask" : ["wall" ] },
        /* 69 */ { "x" : 0, "y" : 2.5, "cMask" : ["wall" ] },
        /* 70 */ { "x" : 405, "y" : -2.5, "cMask" : ["wall" ] },
        /* 71 */ { "x" : 405, "y" : 2.5, "cMask" : ["wall" ] },
        /* 72 */ { "x" : -555, "y" : -80, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 73 */ { "x" : -555, "y" : 80, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ] },
        /* 74 */ { "x" : -550, "y" : -80, "cMask" : ["wall" ] },
        /* 75 */ { "x" : -550, "y" : 80, "cMask" : ["wall" ] },
        /* 76 */ { "x" : -553, "y" : -80, "cMask" : ["wall" ] },
        /* 77 */ { "x" : -553, "y" : 80, "cMask" : ["wall" ] },
        /* 78 */ { "x" : 553, "y" : -80, "cMask" : ["wall" ] },
        /* 79 */ { "x" : 553, "y" : 80, "cMask" : ["wall" ] },
        /* 80 */ { "x" : 550, "y" : -80, "cMask" : ["wall" ] },
        /* 81 */ { "x" : 550, "y" : 80, "cMask" : ["wall" ] },
        /* 82 */ { "x" : 555, "y" : -80, "cMask" : ["wall" ] },
        /* 83 */ { "x" : 555, "y" : 80, "cMask" : ["wall" ] },
        /* 84 */ { "x" : -550, "y" : -80, "cMask" : ["wall" ] },
        /* 85 */ { "x" : -550, "y" : 80, "cMask" : ["wall" ] },
        /* 86 */ { "x" : 550, "y" : 80, "cMask" : ["wall" ] },
        /* 87 */ { "x" : 550, "y" : -80, "cMask" : ["wall" ] }

    ],

    "segments" : [
        { "v0" : 0, "v1" : 1, "cMask" : ["ball" ], "color" : "4D405C" },
        { "v0" : 2, "v1" : 3, "cMask" : ["ball" ], "color" : "4D405C" },
        { "v0" : 4, "v1" : 5, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 6, "v1" : 7, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 8, "v1" : 9, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 10, "v1" : 11, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 12, "v1" : 13, "cMask" : ["ball" ], "color" : "201B26" },
        { "v0" : 14, "v1" : 15, "cMask" : ["ball" ], "color" : "201B26" },
        { "v0" : 16, "v1" : 17, "cMask" : ["wall" ], "color" : "201B26" },
        { "v0" : 18, "v1" : 19, "cMask" : ["wall" ], "color" : "201B26" },
        { "v0" : 20, "v1" : 21, "vis" : false, "cMask" : ["red","blue" ], "cGroup" : ["redKO","blueKO" ], "color" : "453952" },
        { "v0" : 22, "v1" : 23, "vis" : false, "cMask" : ["red","blue" ], "cGroup" : ["redKO","blueKO" ], "color" : "453952" },
        { "v0" : 20, "v1" : 22, "curve" : 180, "curveF" : 6.123233995736766e-17, "cMask" : ["red","blue" ], "cGroup" : ["redKO" ], "color" : "201B26" },
        { "v0" : 25, "v1" : 24, "curve" : 180, "curveF" : 6.123233995736766e-17, "cMask" : ["red","blue" ], "cGroup" : ["blueKO" ], "color" : "201B26" },
        { "v0" : 26, "v1" : 27, "bias" : 10, "vis" : false, "cMask" : ["ball" ], "color" : "453952" },
        { "v0" : 28, "v1" : 29, "bias" : -10, "vis" : false, "cMask" : ["ball" ], "color" : "453952" },
        { "v0" : 30, "v1" : 31, "bias" : 10, "vis" : false, "cMask" : ["ball" ], "color" : "453952" },
        { "v0" : 32, "v1" : 33, "bias" : -10, "vis" : false, "cMask" : ["ball" ], "color" : "453952" },
        { "v0" : 34, "v1" : 35, "bias" : -10, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ], "color" : "201B26" },
        { "v0" : 36, "v1" : 37, "bias" : 10, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ], "color" : "201B26" },
        { "v0" : 38, "v1" : 39, "bias" : -10, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ], "color" : "201B26" },
        { "v0" : 40, "v1" : 41, "bias" : 10, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ], "color" : "201B26" },
        { "v0" : 42, "v1" : 43, "bias" : -10, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ], "color" : "201B26" },
        { "v0" : 44, "v1" : 45, "bias" : 10, "bCoef" : 0.2, "cMask" : ["ball" ], "cGroup" : ["ball" ], "color" : "201B26" },
        { "v0" : 46, "v1" : 47, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 48, "v1" : 49, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 50, "v1" : 51, "curve" : 119.99999999999999, "curveF" : 0.577350269189626, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 52, "v1" : 53, "curve" : 119.99999999999999, "curveF" : 0.577350269189626, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 54, "v1" : 55, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 56, "v1" : 57, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 58, "v1" : 59, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 60, "v1" : 61, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 62, "v1" : 63, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 64, "v1" : 65, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 66, "v1" : 67, "curve" : 180, "curveF" : 6.123233995736766e-17, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 67, "v1" : 66, "curve" : 180, "curveF" : 6.123233995736766e-17, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 67, "v1" : 66, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 68, "v1" : 69, "curve" : 180, "curveF" : 6.123233995736766e-17, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 69, "v1" : 68, "curve" : 180, "curveF" : 6.123233995736766e-17, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 69, "v1" : 68, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 70, "v1" : 71, "curve" : 180, "curveF" : 6.123233995736766e-17, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 71, "v1" : 70, "curve" : 180, "curveF" : 6.123233995736766e-17, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 71, "v1" : 70, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 72, "v1" : 73, "cMask" : ["wall" ], "color" : "3D3542" },
        { "v0" : 74, "v1" : 75, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 76, "v1" : 77, "cMask" : ["wall" ], "color" : "3D3542" },
        { "v0" : 78, "v1" : 79, "cMask" : ["wall" ], "color" : "3D3542" },
        { "v0" : 80, "v1" : 81, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 82, "v1" : 83, "cMask" : ["wall" ], "color" : "3D3542" },
        { "v0" : 84, "v1" : 85, "curve" : 180, "cMask" : ["wall" ], "color" : "4D405C" },
        { "v0" : 86, "v1" : 87, "curve" : 180, "cMask" : ["wall" ], "color" : "4D405C" }

    ],

    "planes" : [
        { "normal" : [0,-1 ], "dist" : -240, "cMask" : ["ball" ] },
        { "normal" : [0,1 ], "dist" : -240, "cMask" : ["ball" ] },
        { "normal" : [1,0 ], "dist" : -625, "cMask" : ["red","blue" ] },
        { "normal" : [0,-1 ], "dist" : -270, "cMask" : ["red","blue" ] },
        { "normal" : [0,1 ], "dist" : -270, "cMask" : ["red","blue" ] },
        { "normal" : [-1,0 ], "dist" : -625, "cMask" : ["red","blue" ] }

    ],

    "goals" : [
        { "p0" : [-555.8,-80 ], "p1" : [-555.8,80 ], "team" : "red" },
        { "p0" : [555.8,-80 ], "p1" : [555.8,80 ], "team" : "blue" }

    ],
    "discs":
    [
        {
            "radius": 5.8,
            "bCoef": 0.412,
            "invMass": 1.5,
            "color": "FFCF4B",
            "cGroup":
            [
                "ball",
                "kick",
                "score"
            ]
        },
        {
            "pos":
            [
                -550,
                -80
            ],
            "radius": 5.4,
            "invMass": 0,
            "color": "201B26"
        },
        {
            "pos":
            [
                -550,
                80
            ],
            "radius": 5.4,
            "invMass": 0,
            "color": "201B26"
        },
        {
            "pos":
            [
                550,
                -80
            ],
            "radius": 5.4,
            "invMass": 0,
            "color": "201B26"
        },
        {
            "pos":
            [
                550,
                80
            ],
            "radius": 5.4,
            "invMass": 0,
            "color": "201B26"
        }
    ],
    "playerPhysics":
    {
        "bCoef": 0,
        "acceleration": 0.1125,
        "kickingAcceleration": 0.095,
        "kickStrength": 4.45
    },
    "ballPhysics": "disc0",
    "spawnDistance": 150
}`;

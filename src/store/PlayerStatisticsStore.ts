import { PlayerStatistics } from "../models/PlayerStatistics";
import { Statistics } from "../models/Statistics";
import {
  azureTableService,
  StatisticsEntity,
} from "../services/AzureTableService";
import { room } from "../index";
import { Configuration } from "../configuration/Configuration";

export class PlayerStatisticsStore {
  private playerStatisticsList: PlayerStatistics[] = [];
  private roomName = Configuration.getRoomName();

  public async saveStatistics(): Promise<void> {
    const statisticsEntities: StatisticsEntity[] = [] as StatisticsEntity[];
    for (const playerStatistics of this.playerStatisticsList) {
      const playerStatisticsJson: string = JSON.stringify(playerStatistics);
      console.log(playerStatistics);

      statisticsEntities.push({
        partitionKey: this.roomName,
        rowKey: playerStatistics.name,
        Statistics: playerStatisticsJson,
      } as StatisticsEntity);
    }

    await azureTableService.updateRoomStatistics(statisticsEntities);
  }

  public async loadScores(): Promise<void> {
    this.playerStatisticsList = [];

    const statisticsEntities: StatisticsEntity[] =
      await azureTableService.getStatistics();

    for (const statisticEntity of statisticsEntities) {
      const name: string = statisticEntity.rowKey;
      const scoresJson: any = JSON.parse(statisticEntity.Statistics) as any;

      if (scoresJson === null || scoresJson.length === 0) {
        continue;
      }

      const gamesPlayed: number = scoresJson._gamesPlayed;
      const cumulativeStatistics = PlayerStatisticsStore.buildStatistics(
        scoresJson._cumulativeStatistics
      );
      const cumulativeWeeklyStatistics = PlayerStatisticsStore.buildStatistics(
        scoresJson._cumulativeWeeklyStatistics
      );
      const lastGameStatistics = PlayerStatisticsStore.buildStatistics(
        scoresJson._lastGameStatistics
      );

      const playerStatistics = new PlayerStatistics(
        name,
        gamesPlayed,
        cumulativeStatistics,
        cumulativeWeeklyStatistics,
        lastGameStatistics
      );
      this.playerStatisticsList.push(playerStatistics);
    }
  }

  private static buildStatistics(statisticsJson: any): Statistics {
    return new Statistics(
      statisticsJson._goalsScored,
      statisticsJson._assists,
      statisticsJson._shotsOnTarget,
      statisticsJson._shotsSaved,
      statisticsJson._ownGoals
    );
  }

  public addPlayer(name: string): void {
    if (this.isPlayerInList(name)) {
      console.log(`player ${name} already in the store!`);
      return;
    }

    this.playerStatisticsList.push(new PlayerStatistics(name));
  }

  public isPlayerInList(name: string): boolean {
    return (
      this.playerStatisticsList.find((player) => player.name === name) !==
      undefined
    );
  }

  public addGamePlayedForAll(): void {
    const players = room.getPlayerList();
    this.playerStatisticsList.forEach((playerStat) => {
      if (
        players.filter((player) => player.name === playerStat.name).length > 0
      ) {
        this.addGamePlayed(playerStat.name);
      }
    });
  }

  public addGamePlayed(playerName: string): void {
    this.getPlayerStatistics(playerName).addGamePlayed();
  }

  public addScore(playerName: string): void {
    this.getPlayerStatistics(playerName).addScore();
  }
  public addShotOnTarget(playerName: string): void {
    this.getPlayerStatistics(playerName).addShotOnTarget();
  }
  public addSave(playerName: string): void {
    this.getPlayerStatistics(playerName).addSave();
  }
  public addAssist(playerName: string): void {
    this.getPlayerStatistics(playerName).addAssist();
  }
  public addOwnGoal(playerName: string): void {
    this.getPlayerStatistics(playerName).addOwnGoal();
  }

  public getGameStatistics(): GameStatistics[] {
    const players = room.getPlayerList();
    return this.playerStatisticsList
      .filter(
        (playerStatistic) =>
          players.find((player) => player.name === playerStatistic.name) !==
          null
      )
      .map(
        (playerStatistic) =>
          ({
            playerName: playerStatistic.name,
            statistics: playerStatistic.lastGameStatistics,
          } as GameStatistics)
      );
  }

  public resetGameStatistics(): void {
    this.playerStatisticsList.forEach((playerStatistics) =>
      playerStatistics.lastGameStatistics.reset()
    );
  }

  public getWeeklyGameStatistics(): GameStatistics[] {
    return this.playerStatisticsList.map(
      (playerStatistic) =>
        ({
          playerName: playerStatistic.name,
          statistics: playerStatistic.cumulativeWeeklyStatistics,
        } as GameStatistics)
    );
  }

  public async resetWeeklyStatistics(): Promise<void> {
    this.playerStatisticsList.forEach((playerStatistics) =>
      playerStatistics.cumulativeWeeklyStatistics.reset()
    );
    await this.saveStatistics();
  }

  private getPlayerStatistics(playerName: string): PlayerStatistics {
    this.ensurePlayerInitialised(playerName);

    return this.playerStatisticsList.find(
      (playerStats) => playerStats.name === playerName
    ) as PlayerStatistics;
  }

  private ensurePlayerInitialised(playerName: string) {
    if (!this.isPlayerInList(playerName)) {
      this.addPlayer(playerName);
    }
  }
}

export interface GameStatistics {
  playerName: string;
  statistics: Statistics;
}

export const playerStatisticsStore = new PlayerStatisticsStore();

import { Stadium, StadiumName } from "../models/Stadium";
import { futsal1Stadium } from "../stadiums/Futsal1Stadium";
import { futsal3Stadium } from "../stadiums/Futsal3Stadium";
import { futsal5Stadium } from "../stadiums/Futsal5Stadium";
import { uefaStadium } from "../stadiums/UEFAStadium";
import { futX4Stadium } from "../stadiums/FutX4";
import { futX5Stadium } from "../stadiums/FutX5";

export class StadiumStore {
  private static readonly FUT_X4_PITCH = futX4Stadium;
  private static readonly FUT_X5_PITCH = futX5Stadium;
  private static readonly FUTSAL_1X1_PITCH = futsal1Stadium;
  private static readonly FUTSAL_3X3_PITCH = futsal3Stadium;
  private static readonly FUTSAL_5X5_PITCH = futsal5Stadium;
  private static readonly UEFA_PITCH = uefaStadium;
  private static readonly UEFA_BALL_SIZE = 10;
  private static readonly FUTSAL_BALL_SIZE = 6.4;
  private static readonly FUT_XX_BALL_SIZE = 5.8;
  private static readonly stadiumList: Stadium[] = StadiumStore.populateStadiumList();

  public static getCurrentStadium(): Stadium | undefined {
    return this.stadiumList.find((stadium) => stadium.currentStadium);
  }

  public static getStadium(stadiumName: StadiumName): Stadium | undefined {
    return this.stadiumList.find((stadium) => stadium.name === stadiumName);
  }

  // Called when stadium is changed manually
  public static trySetCurrentStadium(stadiumName: string): void {
    switch (stadiumName) {
      case "Fut x4":
        this.setCurrentStadium(StadiumName.FUT_X4);
        break;
      case "Fut x5":
        this.setCurrentStadium(StadiumName.FUT_X5);
        break;
      case "Futsal 1x1 2x2":
        this.setCurrentStadium(StadiumName.FUTSAL_1X1);
        break;
      case "Futsal 3x3 4x4":
        this.setCurrentStadium(StadiumName.FUTSAL_3X3);
        break;
      case "Futsal 5x5 6x6":
        this.setCurrentStadium(StadiumName.FUTSAL_5X5);
        break;
      case "UEFA Champions League":
        this.setCurrentStadium(StadiumName.UEFA);
        break;
      default:
        console.log(`Stadium set to unsupported stadium: ${stadiumName}`);
    }
  }

  public static setCurrentStadium(stadiumName: StadiumName): void {
    if (Object.values(StadiumName).includes(stadiumName)) {
      console.log(`Setting stadium: ${stadiumName}`);
      for (const stadium of StadiumStore.stadiumList) {
        stadium.currentStadium = stadium.name === stadiumName;
      }
    }
  }

  private static populateStadiumList(): Stadium[] {
    const futsal1 = this.buildStadium(
      StadiumName.FUTSAL_1X1,
      this.FUTSAL_BALL_SIZE,
      368,
      57,
      [-235, 133],
      [-383, 133],
      [-235, -133],
      [-383, -133],
      [383, 133],
      [235, 133],
      [383, -133],
      [235, -133],
      1,
      this.FUTSAL_1X1_PITCH
    );
    const futsal3 = this.buildStadium(
      StadiumName.FUTSAL_3X3,
      this.FUTSAL_BALL_SIZE,
      665,
      72,
      [-485, 200],
      [-680, 200],
      [-485, -200],
      [-680, -200],
      [680, 200],
      [485, 200],
      [680, -200],
      [485, -200],
      2,
      this.FUTSAL_3X3_PITCH
    );
    const futsal5 = this.buildStadium(
      StadiumName.FUTSAL_5X5,
      this.FUTSAL_BALL_SIZE,
      793,
      87,
      [-557, 235],
      [-808, 235],
      [-557, -235],
      [-808, -235],
      [808, 235],
      [557, 235],
      [808, -235],
      [557, -235],
      3,
      this.FUTSAL_5X5_PITCH
    );
    const uefa = this.buildStadium(
      StadiumName.UEFA,
      this.UEFA_BALL_SIZE,
      550,
      72,
      [-345, 195],
      [-565, 195],
      [-345, -195],
      [-565, -195],
      [565, 195],
      [345, 195],
      [565, -195],
      [345, -195],
      2,
      this.UEFA_PITCH
    );
    const futX4 = this.buildStadium(
      StadiumName.FUT_X4,
      this.FUT_XX_BALL_SIZE,
      550,
      80,
      [-370, 160],
      [-550, 160],
      [-370, -160],
      [-550, -160],
      [550, 160],
      [370, 160],
      [550, -160],
      [370, -160],
      2,
      this.FUT_X4_PITCH
    );
    const futX5 = this.buildStadium(
      StadiumName.FUT_X5,
      this.FUT_XX_BALL_SIZE,
      715,
      80,
      [-530, 160],
      [-710, 160],
      [-530, -160],
      [-710, -160],
      [710, 160],
      [530, 160],
      [710, -160],
      [530, -160],
      2,
      this.FUT_X5_PITCH
    );

    return [futsal1, futsal3, futsal5, uefa, futX4, futX5];
  }

  private static buildStadium(
    stadiumName: string,
    ballRadius: number,
    goalXMagnitude: number,
    goalYMagnitude: number,
    redDefUpperRightCoord: number[],
    redDefUpperLeftCoord: number[],
    redDefLowerRightCoord: number[],
    redDefLowerLeftCoord: number[],
    blueDefUpperRightCoord: number[],
    blueDefUpperLeftCoord: number[],
    blueDefLowerRightCoord: number[],
    blueDefLowerLeftCoord: number[],
    defaultMaxDefenders: number,
    pitchData: string
  ): Stadium {
    return {
      name: stadiumName,
      ballRadius: ballRadius,
      currentStadium: false,
      goalAreas: {
        red: {
          xCoord: goalXMagnitude * -1,
          yUpperCoord: goalYMagnitude,
          yLowerCoord: goalYMagnitude * -1,
        },
        blue: {
          xCoord: goalXMagnitude,
          yUpperCoord: goalYMagnitude,
          yLowerCoord: goalYMagnitude * -1,
        },
      },
      defenceBoundary: {
        red: {
          upperLeftCoord: {
            x: redDefUpperLeftCoord[0],
            y: redDefUpperLeftCoord[1],
          },
          upperRightCoord: {
            x: redDefUpperRightCoord[0],
            y: redDefUpperRightCoord[1],
          },
          lowerLeftCoord: {
            x: redDefLowerLeftCoord[0],
            y: redDefLowerLeftCoord[1],
          },
          lowerRightCoord: {
            x: redDefLowerRightCoord[0],
            y: redDefLowerRightCoord[1],
          },
        },
        blue: {
          upperLeftCoord: {
            x: blueDefUpperLeftCoord[0],
            y: blueDefUpperLeftCoord[1],
          },
          upperRightCoord: {
            x: blueDefUpperRightCoord[0],
            y: blueDefUpperRightCoord[1],
          },
          lowerLeftCoord: {
            x: blueDefLowerLeftCoord[0],
            y: blueDefLowerLeftCoord[1],
          },
          lowerRightCoord: {
            x: blueDefLowerRightCoord[0],
            y: blueDefLowerRightCoord[1],
          },
        },
      },
      defaultMaxDefenders: defaultMaxDefenders,
      pitchData: pitchData,
    } as Stadium;
  }
}

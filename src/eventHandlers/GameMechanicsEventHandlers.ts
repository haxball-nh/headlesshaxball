import { Player } from "../models/haxball/Player";
import { Team } from "../models/haxball/Team";
import { Scores } from "../models/haxball/Scores";
import { room } from "../index";
import { announcementService } from "../services/AnnouncementService";
import { ballService } from "../services/BallService";
import { goalService } from "../services/GoalService";
import { collisionService } from "../services/CollisionService";
import { playerStatisticsStore } from "../store/PlayerStatisticsStore";
import { azureTableService } from "../services/AzureTableService";
import { rulesService } from "../services/RulesService";
import { roomManagementService } from "../services/RoomManagementService";
import { StadiumStore } from "../store/StadiumStore";

export const registerGameMechanicsEventHandlers = () => {
  registerOnPlayerBallKick();
  registerOnTeamGoal();
  registerOnGameTick();
  registerOnTeamVictory();
  registerOnGameStart();
  registerOnGameStop();
  registerOnRoomLink();
  registerOnStadiumChange();
};

const registerOnPlayerBallKick = () => {
  room.onPlayerBallKick = (player: Player) => {
    try {
      ballService.setBallKicked(player);
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnTeamGoal = () => {
  room.onTeamGoal = async (team: Team) => {
    try {
      ballService.setGoalCalcInProgress(true);
      await goalService.handleGoal(team);
      ballService.resetBall();
      ballService.setGoalCalcInProgress(false);
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnGameTick = () => {
  room.onGameTick = () => {
    try {
      collisionService.updateBallPlayerCollisions();
      rulesService.enforceMaxDefenders();
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnTeamVictory = () => {
  room.onTeamVictory = async (_: Scores) => {
    try {
      announcementService.announceGameScores();
      playerStatisticsStore.addGamePlayedForAll();
      await playerStatisticsStore.saveStatistics();
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnGameStart = () => {
  room.onGameStart = (_: Player) => {
    try {
      ballService.resetBall();
      playerStatisticsStore.resetGameStatistics();
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnGameStop = () => {
  room.onGameStop = (_: Player) => {
    try {
      roomManagementService.updateRoom();
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnRoomLink = () => {
  room.onRoomLink = async (url: string) => {
    try {
      await azureTableService.updateUrl(url);
      await playerStatisticsStore.loadScores();
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnStadiumChange = () => {
  room.onStadiumChange = (stadiumName: string, _: Player) => {
    try {
      StadiumStore.trySetCurrentStadium(stadiumName);
    } catch (error) {
      console.log(error);
    }
  };
};

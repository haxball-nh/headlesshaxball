import { roomManagementService } from "../services/RoomManagementService";
import { Player } from "../models/haxball/Player";
import { chatCommandService } from "../services/ChatCommandService";
import { room } from "../index";
import { announcementService } from "../services/AnnouncementService";

export const registerPlayerEventHandlers = () => {
  registerOnPlayerJoin();
  registerOnPlayerLeave();
  registerOnPlayerChat();
  registerOnPlayerAdminChange();
};

const registerOnPlayerJoin = () => {
  room.onPlayerJoin = (_: Player) => {
    try {
      roomManagementService.updateRoom();
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnPlayerLeave = () => {
  room.onPlayerLeave = (_: Player) => {
    try {
      roomManagementService.updateRoom();
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnPlayerChat = () => {
  room.onPlayerChat = (player: Player, message: string) => {
    try {
      chatCommandService.playerChat(player, message);
    } catch (error) {
      console.log(error);
    }
  };
};
const registerOnPlayerAdminChange = () => {
  room.onPlayerAdminChange = (changedPlayer: Player, byPlayer: Player) => {
    try {
      if (
        !changedPlayer.admin &&
        roomManagementService.isPlayerOnAdminList(changedPlayer)
      ) {
        roomManagementService.setAdmin(changedPlayer, true);
        roomManagementService.setAvatar(byPlayer.id, "🤡");
        announcementService.sendAnnouncement(
          `You can't do that ${byPlayer.name}`
        );
      }
    } catch (error) {
      console.log(error);
    }
  };
};

import { Statistics } from "./Statistics";

export class PlayerStatistics {
  private readonly _name: string;
  private _gamesPlayed: number;
  private readonly _cumulativeStatistics: Statistics;
  private readonly _cumulativeWeeklyStatistics: Statistics;
  private readonly _lastGameStatistics: Statistics;

  constructor(
    name: string,
    gamesPlayed = 0,
    cumulativeStatistics: Statistics = new Statistics(),
    cumulativeWeeklyStatistics: Statistics = new Statistics(),
    lastGameStatistics: Statistics = new Statistics()
  ) {
    this._name = name;
    this._gamesPlayed = gamesPlayed;
    this._cumulativeStatistics = cumulativeStatistics;
    this._cumulativeWeeklyStatistics = cumulativeWeeklyStatistics;
    this._lastGameStatistics = lastGameStatistics;
  }

  get name(): string {
    return this._name;
  }
  get gamesPlayed(): number {
    return this._gamesPlayed;
  }
  get cumulativeStatistics(): Statistics {
    return this._cumulativeStatistics;
  }
  get cumulativeWeeklyStatistics(): Statistics {
    return this._cumulativeWeeklyStatistics;
  }
  get lastGameStatistics(): Statistics {
    return this._lastGameStatistics;
  }

  public addGamePlayed(): void {
    this._gamesPlayed++;
  }

  public addScore(): void {
    this._cumulativeStatistics.addScore();
    this._cumulativeWeeklyStatistics.addScore();
    this._lastGameStatistics.addScore();
  }
  public addShotOnTarget(): void {
    this._cumulativeStatistics.addShotOnTarget();
    this._cumulativeWeeklyStatistics.addShotOnTarget();
    this._lastGameStatistics.addShotOnTarget();
  }
  public addSave(): void {
    this._cumulativeStatistics.addSave();
    this._cumulativeWeeklyStatistics.addSave();
    this._lastGameStatistics.addSave();
  }
  public addAssist(): void {
    this._cumulativeStatistics.addAssist();
    this._cumulativeWeeklyStatistics.addAssist();
    this._lastGameStatistics.addAssist();
  }
  public addOwnGoal(): void {
    this._cumulativeStatistics.addOwnGoal();
    this._cumulativeWeeklyStatistics.addOwnGoal();
    this._lastGameStatistics.addOwnGoal();
  }

  public resetGameStatistics(): void {
    this._lastGameStatistics.reset();
  }

  public resetWeeklyStatistics(): void {
    this._cumulativeWeeklyStatistics.reset();
  }
}

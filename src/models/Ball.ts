import { Player } from "./haxball/Player";

export interface Ball {
  kickedBy: BallKickedBy;
  touchedBy: BallTouchedBy;
  assistingPlayer: Player | null;
}

export interface BallKickedBy {
  player: Player | null;
  time: number;
  onTarget: boolean;
}

export interface BallTouchedBy {
  player: Player | null;
  time: number;
}

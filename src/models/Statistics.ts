export class Statistics {
  private _goalsScored: number;
  private _assists: number;
  private _shotsOnTarget: number;
  private _shotsSaved: number;
  private _ownGoals: number;

  constructor(
    goalsScored = 0,
    assists = 0,
    shotsOnTarget = 0,
    shotsSaved = 0,
    ownGoals = 0
  ) {
    this._goalsScored = goalsScored;
    this._assists = assists;
    this._shotsOnTarget = shotsOnTarget;
    this._shotsSaved = shotsSaved;
    this._ownGoals = ownGoals;
  }

  public get goalsScored(): number {
    return this._goalsScored;
  }
  public get assists(): number {
    return this._assists;
  }
  public get shotsOnTarget(): number {
    return this._shotsOnTarget;
  }
  public get shotsSaved(): number {
    return this._shotsSaved;
  }

  public get ownGoals(): number {
    return this._ownGoals;
  }

  public addScore(): void {
    this._goalsScored++;
  }

  public addShotOnTarget(): void {
    this._shotsOnTarget++;
  }

  public addSave(): void {
    this._shotsSaved++;
  }
  public addAssist(): void {
    this._assists++;
  }
  public addOwnGoal(): void {
    this._ownGoals++;
  }
  public reset(): void {
    this._goalsScored = 0;
    this._assists = 0;
    this._shotsOnTarget = 0;
    this._shotsSaved = 0;
    this._ownGoals = 0;
  }
}

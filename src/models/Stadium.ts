export interface Stadium {
  name: StadiumName;
  ballRadius: number;
  goalAreas: {
    red: {
      xCoord: number;
      yUpperCoord: number;
      yLowerCoord: number;
    };
    blue: {
      xCoord: number;
      yUpperCoord: number;
      yLowerCoord: number;
    };
  };
  defenceBoundary: {
    red: DefenceArea;
    blue: DefenceArea;
  };
  defaultMaxDefenders: number;
  pitchData: string;
  currentStadium: boolean;
}

export interface DefenceArea {
  upperLeftCoord: {
    x: number;
    y: number;
  };
  upperRightCoord: {
    x: number;
    y: number;
  };
  lowerLeftCoord: {
    x: number;
    y: number;
  };
  lowerRightCoord: {
    x: number;
    y: number;
  };
}

export enum StadiumName {
  FUTSAL_1X1 = "FUTSAL_1X1",
  FUTSAL_3X3 = "FUTSAL_3X3",
  FUTSAL_5X5 = "FUTSAL_5X5",
  UEFA = "UEFA",
  FUT_X4 = "FUT_X4",
  FUT_X5 = "FUT_X5",
}

import { Player } from "./Player";
import { Disc } from "./Disc";
import { Scores } from "./Scores";
import { Team } from "./Team";

export interface Room {
  // Commands
  sendChat(message: string, targetId?: number): void;
  setPlayerAdmin(playerID: number, admin: boolean): void;
  setPlayerTeam(playerID: number, team: number): void;
  kickPlayer(playerID: number, reason: string, ban: boolean): void;
  clearBan(playerId: number): void;
  clearBans(): void;
  setScoreLimit(scoreLimit: number): void;
  setTimeLimit(timeLimit: number): void;
  setCustomStadium(stadiumFileContents: string): void;
  setDefaultStadium(stadiumName: string): void;
  setTeamsLock(locked: boolean): void;
  setTeamColors(
    team: Team,
    angle: number,
    textColor: number,
    colors: number[]
  ): void;
  startGame(): void;
  stopGame(): void;
  pauseGame(pauseState: boolean): void;
  getPlayer(playerId: number): Player;
  getPlayerList(): Player[];
  getBallPosition(): { x: number; y: number };
  startRecording(): void;
  stopRecording(): Uint8Array;
  setPassword(pass: string): void;
  setRequireRecaptcha(required: boolean): void;
  reorderPlayers(playerIdList: number[], moveToTop: boolean): void;
  sendAnnouncement(
    msg: string,
    targetId?: number,
    color?: number,
    style?: string,
    sound?: number
  ): void;
  setKickRateLimit(min: number, rate: number, burst: number): void;
  setPlayerAvatar(playerId: number, avatar: string | null): void;
  setDiscProperties(discIndex: number, properties: Disc): void;
  getDiscProperties(discIndex: number): Disc;
  setPlayerDiscProperties(playerId: number, properties: Disc): void;
  getPlayerDiscProperties(playerId: number): Disc;
  getDiscCount(): number;

  //Events
  onPlayerJoin(player: Player): void;
  onPlayerLeave(player: Player): void;
  onTeamVictory(scores: Scores): void;
  onPlayerChat(player: Player, message: string): void;
  onPlayerBallKick(player: Player): void;
  onTeamGoal(team: Team): void;
  onGameStart(byPlayer: Player): void;
  onGameStop(byPlayer: Player): void;
  onPlayerAdminChange(changedPlayer: Player, byPlayer: Player): void;
  onPlayerTeamChange(changedPlayer: Player, byPlayer: Player): void;
  onPlayerKicked(
    kickedPlayer: Player,
    reason: string,
    ban: boolean,
    byPlayer: Player
  ): void;

  onGameTick(): void;
  onGamePause(byPlayer: Player): void;
  onGameUnpause(byPlayer: Player): void;
  onPositionsReset(): void;
  onPlayerActivity(player: Player): void;
  onStadiumChange(newStadiumName: string, byPlayer: Player): void;
  onRoomLink(url: string): void;
  onKickRateLimitSet(
    min: number,
    rate: number,
    burst: number,
    byPlayer: Player
  ): void;
}

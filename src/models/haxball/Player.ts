import { Team } from "./Team";

export interface Player {
  id: number;
  name: string;
  team: Team;
  admin: boolean;
  position: {
    x: number;
    y: number;
  };
  auth: string;
  conn: string;
}

export enum Team {
  Spectators = 0,
  RedTeam = 1,
  BlueTeam = 2,
}

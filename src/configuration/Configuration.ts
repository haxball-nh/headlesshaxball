export class Configuration {
  public static getPassword(): string {
    //@ts-ignore
    return PASSWORD();
  }
  public static getRoomName(): string {
    //@ts-ignore
    return ROOM_NAME();
  }
  public static getSasToken(): string {
    //@ts-ignore
    return SAS_TOKEN();
  }
  public static getToken(): string {
    //@ts-ignore
    return TOKEN();
  }
  public static getTableUrl(): string {
    //@ts-ignore
    return TABLE_URL();
  public static getAdminIds(): string {
    //@ts-ignore
    return ADMIN_IDS();
  }
}

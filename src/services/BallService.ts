import { Ball, BallKickedBy, BallTouchedBy } from "../models/Ball";
import { Player } from "../models/haxball/Player";
import { BallUtility } from "../Utilities/BallUtility";
import { shotService } from "./ShotService";
import { playerStatisticsStore } from "../store/PlayerStatisticsStore";
import { announcementService } from "./AnnouncementService";
import { RulesService } from "./RulesService";
import { DefenceArea, Stadium } from "../models/Stadium";
import { StadiumStore } from "../store/StadiumStore";
import { Team } from "../models/haxball/Team";
import { room } from "../index";

export class BallService {
  private readonly ball: Ball;
  private goalCalcInProgress = false;
  private shotJustSaved = false;

  constructor() {
    this.ball = {
      kickedBy: {
        player: null,
        time: 0,
        onTarget: false,
      },
      touchedBy: {
        player: null,
        time: 0,
      },
      assistingPlayer: null,
    } as Ball;
  }

  public setBallKicked(player: Player): void {
    if (this.isShotSaved(player)) {
      this.shotJustSaved = true;
      playerStatisticsStore.addSave(player.name);
      if (this.ball.kickedBy.player) {
        announcementService.announceSave(player, this.ball.kickedBy.player);
      }
      this.resetBall();
      this.shotJustSaved = false;
    } else {
      this.updateAssistingPlayer(player);
      const onTarget = shotService.isShotOnTarget(player);
      if (onTarget) {
        playerStatisticsStore.addShotOnTarget(player.name);
      }

      this.ball.kickedBy.player = player;
      this.ball.kickedBy.time = Date.now();
      this.ball.kickedBy.onTarget = onTarget;
    }
  }

  public getBallKicked(): BallKickedBy {
    return this.ball.kickedBy;
  }

  public setBallTouchedBy(player: Player): void {
    if (this.isShotSaved(player)) {
      this.shotJustSaved = true;
      playerStatisticsStore.addSave(player.name);
      if (this.ball.kickedBy.player) {
        announcementService.announceSave(player, this.ball.kickedBy.player);
      }
      this.resetBall();
      this.shotJustSaved = false;
    } else {
      this.updateAssistingPlayer(player);
      this.ball.touchedBy.player = player;
      this.ball.touchedBy.time = Date.now();
    }
  }

  public getBallTouchedBy(): BallTouchedBy {
    return this.ball.touchedBy;
  }

  public getAssistingPlayer(): Player | null {
    return this.ball.assistingPlayer;
  }

  public setGoalCalcInProgress(inProgress: boolean): void {
    this.goalCalcInProgress = inProgress;
  }

  public resetBall(): void {
    this.ball.kickedBy.player = null;
    this.ball.kickedBy.time = 0;
    this.ball.kickedBy.onTarget = false;
    this.ball.touchedBy.player = null;
    this.ball.touchedBy.time = 0;
    this.ball.assistingPlayer = null;
  }
  private updateAssistingPlayer(player: Player): void {
    const lastContact: Player | null = BallUtility.getLastBallContact(
      this.ball.kickedBy,
      this.ball.touchedBy
    );

    if (
      lastContact !== null &&
      lastContact.team === player.team &&
      lastContact.name !== player.name
    ) {
      this.ball.assistingPlayer = lastContact;
    } else {
      this.ball.assistingPlayer = null;
    }
  }
  private isShotSaved(player: Player): boolean {
    const currentStadium: Stadium = StadiumStore.getCurrentStadium() as Stadium;
    const playerDisc = room.getPlayerDiscProperties(player.id);
    let teamDefenseBoundary = {} as DefenceArea;
    switch (player.team) {
      case Team.BlueTeam:
        teamDefenseBoundary = currentStadium.defenceBoundary.blue;
        break;
      case Team.RedTeam:
        teamDefenseBoundary = currentStadium.defenceBoundary.red;
        break;
    }

    // Ball is saved if:
    // goalscorers etc. being calculated, original shot was on target,
    // the shooting player was from the opposing team and the shot is no longer still on target
    return (
      !this.goalCalcInProgress &&
      !this.shotJustSaved &&
      this.ball.kickedBy.player !== player &&
      this.ball.touchedBy.player !== player &&
      this.ball.kickedBy.onTarget &&
      this.ball.kickedBy.player !== null &&
      this.ball.kickedBy.player.team !== player.team &&
      RulesService.playerInDefenceArea(playerDisc, teamDefenseBoundary) &&
      !shotService.isShotOnTarget(this.ball.kickedBy.player)
    );
  }
}

export const ballService = new BallService();

import { playerStatisticsStore } from "../store/PlayerStatisticsStore";

export const registerIntervalActions = () => {
  registerWeeklyStatisticsReset();
};

const registerWeeklyStatisticsReset = () => {
  const weeklyScoreReset = async () => {
    try {
      //reset on Sunday
      if (new Date().getUTCDay() === 0) {
        console.log("It's Sunday, resetting the stats.");
        await playerStatisticsStore.resetWeeklyStatistics();
      }
    } catch (e) {
      console.log(e);
    }
  };
  // check every 12 hours
  setInterval(weeklyScoreReset, 1000 * 60 * 60 * 12);
};

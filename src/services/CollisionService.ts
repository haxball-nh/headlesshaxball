import { StadiumStore } from "../store/StadiumStore";
import { room } from "../index";
import { ballService } from "./BallService";

export class CollisionService {
  public updateBallPlayerCollisions(): void {
    const currentStadium = StadiumStore.getCurrentStadium();
    if (currentStadium === undefined) {
      return;
    }

    const players = room.getPlayerList();
    const ballPosition = room.getBallPosition();
    const playerRadius = 15;
    const triggerDistance = currentStadium.ballRadius + playerRadius + 0.01;

    for (const player of players) {
      if (!player.position) continue;

      const distanceToBall = CollisionService.pointDistance(
        player.position,
        ballPosition
      );

      if (distanceToBall < triggerDistance) {
        ballService.setBallTouchedBy(player);
      }
    }
  }
  private static pointDistance(
    point1: { x: number; y: number },
    point2: { x: number; y: number }
  ): number {
    const xDist = point1.x - point2.x;
    const yDist = point1.y - point2.y;
    return Math.sqrt(xDist * xDist + yDist * yDist);
  }
}

export const collisionService = new CollisionService();

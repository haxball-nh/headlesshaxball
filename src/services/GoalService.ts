import { Player } from "../models/haxball/Player";
import { Team } from "../models/haxball/Team";
import { BallKickedBy, BallTouchedBy } from "../models/Ball";
import { BallUtility } from "../Utilities/BallUtility";
import { playerStatisticsStore } from "../store/PlayerStatisticsStore";
import { ballService } from "./BallService";
import { announcementService } from "./AnnouncementService";
import { roomManagementService } from "./RoomManagementService";

export class GoalService {
  public async handleGoal(team: Team): Promise<void> {
    const ballKickedBy: BallKickedBy = ballService.getBallKicked();
    const ballTouchedBy: BallTouchedBy = ballService.getBallTouchedBy();
    const assistBy: Player | null = ballService.getAssistingPlayer();

    const scoringPlayer: Player | null = GoalService.getScoringPlayer(
      team,
      ballKickedBy,
      ballTouchedBy
    );

    if (scoringPlayer !== null) {
      if (scoringPlayer.team === team) {
        await this.awardGoal(scoringPlayer, assistBy);
        if (scoringPlayer !== ballKickedBy.player || !ballKickedBy.onTarget) {
          playerStatisticsStore.addShotOnTarget(scoringPlayer.name);
        }
      } else {
        await this.awardOwnGoal(scoringPlayer);
      }
    }
  }

  private static getScoringPlayer(
    team: Team,
    ballKickedBy: BallKickedBy,
    ballTouchedBy: BallTouchedBy
  ): Player | null {
    if (ballKickedBy.player === null && ballTouchedBy.player === null) {
      return null;
    }
    if (ballKickedBy.player === null && ballTouchedBy.player !== null) {
      return ballTouchedBy.player;
    }
    if (ballKickedBy.player !== null && ballTouchedBy.player === null) {
      return ballKickedBy.player;
    }

    // if both contacts are by same team return last contact
    if (ballKickedBy.player?.team === ballTouchedBy.player?.team) {
      return BallUtility.getLastBallContact(ballKickedBy, ballTouchedBy);
    }

    // if ball kicked on target by scoring team
    if (ballKickedBy.player?.team === team) {
      if (ballKickedBy.onTarget) {
        return ballKickedBy.player;
      } else {
        return ballTouchedBy.player;
      }
    }
    return BallUtility.getLastBallContact(ballKickedBy, ballTouchedBy);
  }

  private async awardOwnGoal(scorer: Player): Promise<void> {
    playerStatisticsStore.addOwnGoal(scorer.name);
    announcementService.announceOwnGoal(scorer);

    await this.tempSetAvatar(scorer.id, "💩", 5000);
  }

  private async awardGoal(
    scorer: Player,
    assistingPlayer: Player | null
  ): Promise<void> {
    playerStatisticsStore.addScore(scorer.name);
    const isAssist =
      assistingPlayer !== null && assistingPlayer.team === scorer.team;

    const setAvatarPromises: Promise<void>[] = [];

    if (isAssist) {
      playerStatisticsStore.addAssist((assistingPlayer as Player).name);
      setAvatarPromises.push(
        this.tempSetAvatar((assistingPlayer as Player).id, "🥈", 5000)
      );
    }
    announcementService.announceGoal(scorer, assistingPlayer, isAssist);
    setAvatarPromises.push(this.tempSetAvatar(scorer.id, "🥇", 5000));

    await Promise.all(setAvatarPromises);
  }

  private async tempSetAvatar(
    playerId: number,
    avatar: string,
    ms: number
  ): Promise<void> {
    roomManagementService.setAvatar(playerId, avatar);
    await this.delay(ms);
    roomManagementService.setAvatar(playerId, null);
  }

  private delay(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }
}

export const goalService = new GoalService();

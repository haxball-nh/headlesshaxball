import { TableClient } from "@azure/data-tables";
import { Configuration } from "../configuration/Configuration";

export class AzureTableService {
  private readonly RoomEntityRowKey = "RoomEntity";

  private tableUrl = Configuration.getTableUrl();
  private SASToken = Configuration.getSasToken();
  private roomName = Configuration.getRoomName();
  private client: TableClient = new TableClient(
    `${this.tableUrl}${this.SASToken}`,
    "haxball"
  );

  public async getStatistics(): Promise<StatisticsEntity[]> {
    const statisticsEntities = this.client.listEntities<StatisticsEntity>({
      queryOptions: {
        filter: `PartitionKey eq '${this.roomName}' and RowKey ne '${this.RoomEntityRowKey}'`,
      },
    });
    const entities = [] as StatisticsEntity[];
    for await (const statisticsEntity of statisticsEntities) {
      entities.push(statisticsEntity);
    }
    return entities;
  }

  public async updateRoomStatistics(
    statisticsEntities: StatisticsEntity[]
  ): Promise<void> {
    for (const statisticsEntity of statisticsEntities) {
      await this.client.upsertEntity<StatisticsEntity>(
        statisticsEntity,
        "Replace"
      );
    }
  }

  public async updateUrl(roomUrl: string): Promise<void> {
    const roomEntity: RoomEntity = await this.getRoomEntity();
    roomEntity.RoomUrl = roomUrl;

    await this.client.upsertEntity(roomEntity, "Replace");
  }

  private async getRoomEntity(): Promise<RoomEntity> {
    let roomEntity: RoomEntity;
    try {
      roomEntity = await this.client.getEntity<RoomEntity>(
        this.roomName,
        this.RoomEntityRowKey
      );
    } catch (error) {
      await this.handleRoomCreate();

      roomEntity = await this.client.getEntity<RoomEntity>(
        this.roomName,
        this.RoomEntityRowKey
      );
    }
    return roomEntity;
  }
  private async handleRoomCreate(): Promise<void> {
    const entity: RoomEntity = {
      partitionKey: this.roomName,
      rowKey: this.RoomEntityRowKey,
      RoomUrl: "PLACEHOLDER",
    } as RoomEntity;
    await this.client.createEntity(entity);
  }
}

export interface RoomEntity {
  partitionKey: string; //room name
  rowKey: string; //room entity const
  RoomUrl: string; //room url
}

export interface StatisticsEntity {
  partitionKey: string; //room name
  rowKey: string; //player name
  Statistics: string; //stats
}

export const azureTableService = new AzureTableService();

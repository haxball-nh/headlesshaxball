import { announcementService } from "./AnnouncementService";
import { StadiumName } from "../models/Stadium";
import { roomManagementService } from "./RoomManagementService";
import { Player } from "../models/haxball/Player";
import { rulesService } from "./RulesService";

export class ChatCommandService {
  public playerChat(player: Player, message: string): boolean {
    if (message === "!help") {
      ChatCommandService.helpMessage();
    } else if (message.startsWith("!pitch")) {
      ChatCommandService.changePitch(message);
    } else if (message.startsWith("!x4")) {
      roomManagementService.setStadium(StadiumName.FUT_X4);
    } else if (message.startsWith("!x5")) {
      roomManagementService.setStadium(StadiumName.FUT_X5);
    } else if (message.startsWith("!f1")) {
      roomManagementService.setStadium(StadiumName.FUTSAL_1X1);
    } else if (message.startsWith("!f3")) {
      roomManagementService.setStadium(StadiumName.FUTSAL_3X3);
    } else if (message.startsWith("!f5")) {
      roomManagementService.setStadium(StadiumName.FUTSAL_5X5);
    } else if (message.startsWith("!ue")) {
      roomManagementService.setStadium(StadiumName.UEFA);
    } else if (
      message === "!leaderboard" ||
      message === "!l" ||
      message === "!stats"
    ) {
      announcementService.announceWeeklyScores();
    } else if (message === "!reset") {
      roomManagementService.resetRoom();
    } else if (message === "!rules") {
      const rulesEnabled = !rulesService.getRulesEnabled();
      rulesService.setRulesEnabled(rulesEnabled);
      announcementService.sendAnnouncement(
        `Setting rules enabled to: ${rulesEnabled}`
      );
    } else if (message === "!savesEnabled") {
      const savesEnabled = !announcementService.getSavesEnabled();
      announcementService.setSavesEnabled(savesEnabled);
      announcementService.sendAnnouncement(
        `Setting saves enabled to: ${savesEnabled}`
      );
    } else if (message.startsWith("!def")) {
      ChatCommandService.setMaxDefenders(message);
    } else if (message.startsWith("!3/3")) {
      roomManagementService.setGameLimits(3, 3);
    } else if (message.startsWith("!random")) {
      roomManagementService.setRandomTeams();
    } else if (message.startsWith("!rand3")) {
      roomManagementService.setGameLimits(3, 3);
      roomManagementService.setRandomTeams();
    }
    return true;
  }

  private static helpMessage(): void {
    announcementService.sendAnnouncement(
      `You have the following commands available:
    !pitch <pitchName>
    !leaderboard
    !reset
    !rules (toggle rules)
    !savesEnabled (toggle save announcements)
    !def <numDefenders> (set max allowed defenders)
    !3/3 (3min/3goal game)
    !random (random teams)
    !rand3 (3/3 game with random teams)
    
    Available pitches:
    - Fut x4 (aliases: x4)
    - Fut x5 (aliases: x5)
    - Futsal 1x1 2x2 (aliases: futsal 1x1, futsal1, f1)
    - Futsal 3x3 4x4 (aliases: futsal 3x3, futsal3, f3)
    - Futsal 5x5 6x6 (aliases: futsal 5x5, futsal5, f5)
    - UEFA (aliases: u)`
    );
  }
  private static changePitch(message: string): void {
    const arg = message.replace("!pitch ", "").toLowerCase();
    switch (arg) {
      case "futsal 1x1 2x2":
      case "futsal 1x1":
      case "futsal1":
      case "f1":
        roomManagementService.setStadium(StadiumName.FUTSAL_1X1);
        break;
      case "futsal 3x3 4x4":
      case "futsal 3x3":
      case "futsal3":
      case "f3":
        roomManagementService.setStadium(StadiumName.FUTSAL_3X3);
        break;
      case "futsal 5x5 6x6":
      case "futsal 5x5":
      case "futsal5":
      case "f5":
        roomManagementService.setStadium(StadiumName.FUTSAL_5X5);
        break;
      case "uefa":
      case "u":
        roomManagementService.setStadium(StadiumName.UEFA);
        break;
      case "fut x4":
      case "x4":
        roomManagementService.setStadium(StadiumName.FUT_X4);
        break;
      case "fut x5":
      case "x5":
        roomManagementService.setStadium(StadiumName.FUT_X5);
        break;
    }
  }

  private static setMaxDefenders(message: string): void {
    const arg = message.replace("!def ", "").toLowerCase();
    const maxDefenders = +arg;
    rulesService.setMaxDefenders(maxDefenders);
    announcementService.sendAnnouncement(
      `Setting max defenders to: ${maxDefenders}`
    );
  }
}

export const chatCommandService = new ChatCommandService();

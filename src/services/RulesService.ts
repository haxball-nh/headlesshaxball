import { room } from "../index";
import { Player } from "../models/haxball/Player";
import { Team } from "../models/haxball/Team";
import { StadiumStore } from "../store/StadiumStore";
import { DefenceArea, Stadium } from "../models/Stadium";
import { Disc } from "../models/haxball/Disc";

export class RulesService {
  private static maxDefenders: number;
  private static rulesEnabled = false;

  public enforceMaxDefenders(): void {
    if (RulesService.rulesEnabled) {
      this.enforceRedDefenders();
      this.enforceBlueDefenders();
    }
  }
  public setRulesEnabled(rulesEnabled: boolean): void {
    RulesService.rulesEnabled = rulesEnabled;
  }
  public getRulesEnabled(): boolean {
    return RulesService.rulesEnabled;
  }
  public setMaxDefenders(maxDefenders: number): void {
    RulesService.maxDefenders = maxDefenders;
  }

  private enforceRedDefenders(): void {
    const currentStadium: Stadium = StadiumStore.getCurrentStadium() as Stadium;
    const cf = (room as any).CollisionFlags;
    const redBoundary = currentStadium.defenceBoundary.red;

    const redPlayers: Player[] = room
      .getPlayerList()
      .filter((player) => player.team === Team.RedTeam);

    const playersInDefenceArea: Player[] = redPlayers.filter((player) =>
      RulesService.playerInDefenceArea(
        room.getPlayerDiscProperties(player.id),
        redBoundary
      )
    );

    if (playersInDefenceArea.length >= RulesService.maxDefenders) {
      redPlayers
        .filter(
          (player) =>
            !RulesService.playerInDefenceArea(
              room.getPlayerDiscProperties(player.id),
              redBoundary
            )
        )
        .forEach((player) => RulesService.addDefGroup(player, cf.c0));
    } else {
      redPlayers.forEach((player) => RulesService.setGroup(player, 2, cf.c0));
    }
  }

  private enforceBlueDefenders(): void {
    const currentStadium: Stadium = StadiumStore.getCurrentStadium() as Stadium;
    const cf = (room as any).CollisionFlags;
    const blueBoundary = currentStadium.defenceBoundary.blue;

    const bluePlayers: Player[] = room
      .getPlayerList()
      .filter((player) => player.team === Team.BlueTeam);

    const playersInDefenceArea: Player[] = bluePlayers.filter((player) =>
      RulesService.playerInDefenceArea(
        room.getPlayerDiscProperties(player.id),
        blueBoundary
      )
    );

    if (playersInDefenceArea.length >= RulesService.maxDefenders) {
      bluePlayers
        .filter(
          (player) =>
            !RulesService.playerInDefenceArea(
              room.getPlayerDiscProperties(player.id),
              blueBoundary
            )
        )
        .forEach((player) => RulesService.addDefGroup(player, cf.c1));
    } else {
      bluePlayers.forEach((player) => RulesService.setGroup(player, 4, cf.c1));
    }
  }

  public static playerInDefenceArea(
    player: Disc,
    defenceArea: DefenceArea
  ): boolean {
    // x coords
    const inAreaX =
      player.x > defenceArea.upperLeftCoord.x &&
      player.x > defenceArea.lowerLeftCoord.x &&
      player.x < defenceArea.upperRightCoord.x &&
      player.x < defenceArea.lowerRightCoord.x;

    // y coords
    const inAreaY =
      player.y > defenceArea.lowerRightCoord.y &&
      player.y > defenceArea.lowerLeftCoord.y &&
      player.y < defenceArea.upperRightCoord.y &&
      player.y < defenceArea.upperLeftCoord.y;

    return inAreaX && inAreaY;
  }

  private static addDefGroup(player: Player, cf: any): void {
    const playerDiscProperties = room.getPlayerDiscProperties(player.id);
    const hasC0Flag = (playerDiscProperties.cGroup & cf) != 0;

    if (!hasC0Flag) {
      playerDiscProperties.cGroup = playerDiscProperties.cGroup | cf;
      room.setPlayerDiscProperties(player.id, playerDiscProperties);
    }
  }

  private static setGroup(player: Player, group: number, cf: any): void {
    const playerDiscProperties = room.getPlayerDiscProperties(player.id);
    const hasCollisionFlag = (playerDiscProperties.cGroup & cf) != 0;

    if (hasCollisionFlag) {
      playerDiscProperties.cGroup = group;
      room.setPlayerDiscProperties(player.id, playerDiscProperties);
    }
  }
}

export const rulesService = new RulesService();

import { StadiumStore } from "../store/StadiumStore";
import { StadiumName } from "../models/Stadium";
import { Team } from "../models/haxball/Team";
import { room } from "../index";
import { rulesService } from "./RulesService";
import { announcementService } from "./AnnouncementService";
import { Player } from "../models/haxball/Player";
import { Configuration } from "../configuration/Configuration";

export class RoomManagementService {
  public resetRoom(): void {
    const nantBlue = 0x1e4288;
    const nantOrange = 0xf7901e;

    room.setScoreLimit(14);
    room.setTimeLimit(14);
    room.setTeamColors(Team.RedTeam, 60, nantBlue, [nantOrange]);
    room.setTeamColors(Team.BlueTeam, 60, nantOrange, [nantBlue]);

    this.setStadium(StadiumName.FUT_X4);
  }
  public updateRoom(): void {
    const players = room.getPlayerList();

    // Set all players to admin
    players.forEach((player) => {
      room.setPlayerAdmin(player.id, true);
    });

    // Reset room once everyone leaves
    if (players.length === 0) {
      this.resetRoom();
    }

    if (players.length > 8) {
      this.setStadium(StadiumName.FUT_X5);
    }
  }

  public setAdmin(player: Player, admin: boolean): void {
    room.setPlayerAdmin(player.id, admin);
  }
  public isPlayerOnAdminList(player: Player): boolean {
    return (
      Configuration.getAdminIds()
        .split(",")
        .filter((adminId) => adminId === player.auth).length > 0
    );
  }

  public setGameLimits(time: number, score: number): void {
    room.setTimeLimit(time);
    room.setScoreLimit(score);
  }

  public setRandomTeams(): void {
    announcementService.sendAnnouncement("Setting random teams");
    const players = room.getPlayerList();
    let teamChoice = Team.RedTeam;

    while (players.length > 0) {
      const random = Math.floor(Math.random() * players.length);
      const player = players.splice(random, 1)[0];

      switch (teamChoice) {
        case Team.RedTeam:
          room.setPlayerTeam(player.id, Team.RedTeam);
          teamChoice = Team.BlueTeam;
          break;
        case Team.BlueTeam:
          room.setPlayerTeam(player.id, Team.BlueTeam);
          teamChoice = Team.RedTeam;
          break;
      }
    }
  }

  public setStadium(stadiumName: StadiumName): void {
    const stadium = StadiumStore.getStadium(stadiumName);
    if (stadium) {
      rulesService.setMaxDefenders(stadium.defaultMaxDefenders);
      room.setCustomStadium(stadium.pitchData);
      StadiumStore.setCurrentStadium(stadiumName);
    }
  }

  public setAvatar(playerId: number, avatar: string | null): void {
    room.setPlayerAvatar(playerId, avatar);
  }
}

export const roomManagementService = new RoomManagementService();

import { Stadium } from "../models/Stadium";
import { StadiumStore } from "../store/StadiumStore";
import { Player } from "../models/haxball/Player";
import { Team } from "../models/haxball/Team";
import { room } from "../index";

export class ShotService {
  private static readonly goalDepth = 30;

  public isShotOnTarget(player: Player): boolean {
    const currentStadium = StadiumStore.getCurrentStadium();

    if (!currentStadium || !player) {
      return false;
    }

    const shooterTeam = player.team;
    const ball = room.getDiscProperties(0);
    const dampingFactor = ball.damping;
    let posX = ball.x,
      posY = ball.y,
      volX = ball.xspeed,
      volY = ball.yspeed;

    const shotAtRed = shooterTeam === Team.BlueTeam && volX < 0,
      shotAtBlue = shooterTeam === Team.RedTeam && volX > 0;

    if (!shotAtRed && !shotAtBlue) {
      return false;
    }

    // predict finishing location of ball based on initial position and directional velocity
    while (Math.abs(volX) > 0.01 || Math.abs(volY) > 0.01) {
      if (
        (shotAtRed && ShotService.ballInRedGoal(currentStadium, posX, posY)) ||
        (shotAtBlue && ShotService.ballInBlueGoal(currentStadium, posX, posY))
      ) {
        return true;
      } else {
        posX += volX;
        posY += volY;

        volX *= dampingFactor;
        volY *= dampingFactor;
      }
    }
    return false;
  }

  private static ballInRedGoal(
    currentStadium: Stadium,
    posX: number,
    posY: number
  ) {
    const goalFrontX = currentStadium.goalAreas.red.xCoord,
      goalBackX = currentStadium.goalAreas.red.xCoord - this.goalDepth,
      goalUpperY = currentStadium.goalAreas.red.yUpperCoord,
      goalLowerY = currentStadium.goalAreas.red.yLowerCoord;
    return (
      posX < goalFrontX &&
      posX > goalBackX &&
      posY < goalUpperY &&
      posY > goalLowerY
    );
  }
  private static ballInBlueGoal(
    currentStadium: Stadium,
    posX: number,
    posY: number
  ) {
    const goalFrontX = currentStadium.goalAreas.blue.xCoord,
      goalBackX = currentStadium.goalAreas.blue.xCoord + this.goalDepth,
      goalUpperY = currentStadium.goalAreas.blue.yUpperCoord,
      goalLowerY = currentStadium.goalAreas.blue.yLowerCoord;
    return (
      posX > goalFrontX &&
      posX < goalBackX &&
      posY < goalUpperY &&
      posY > goalLowerY
    );
  }
}

export const shotService = new ShotService();

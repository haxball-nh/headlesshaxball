import { Player } from "../models/haxball/Player";
import {
  GameStatistics,
  playerStatisticsStore,
} from "../store/PlayerStatisticsStore";
import { room } from "../index";

export class AnnouncementService {
  private static savesEnabled = true;

  public setSavesEnabled(enabled: boolean): void {
    AnnouncementService.savesEnabled = enabled;
  }
  public getSavesEnabled(): boolean {
    return AnnouncementService.savesEnabled;
  }

  public sendAnnouncement(message: string): void {
    room.sendAnnouncement(message);
  }

  public announceGoal(
    scorer: Player,
    assistingPlayer: Player | null,
    isAssist: boolean
  ): void {
    room.sendAnnouncement(
      scorer.name +
        " scored a goal" +
        (isAssist ? " (assist by " + assistingPlayer?.name + ")" : "")
    );
  }
  public announceOwnGoal(scorer: Player): void {
    const lastGameStatistics: GameStatistics[] = playerStatisticsStore.getGameStatistics();
    const ownGoalsThisGame = lastGameStatistics
      .filter((statistics) => statistics.playerName === scorer.name)
      .map((statistics) => statistics.statistics.ownGoals);

    room.sendAnnouncement(
      `${scorer.name} scored an own goal, they've scored ${ownGoalsThisGame} own goals this game`
    );
    room.sendAnnouncement(`Fuck sake ${scorer.name}!`);
  }

  public announceSave(savingPlayer: Player, shootingPlayer: Player): void {
    if (AnnouncementService.savesEnabled) {
      room.sendAnnouncement(
        `${savingPlayer.name} saved a shot by ${shootingPlayer.name}`
      );
    }
  }

  public announceGameScores(): void {
    const lastGameStatistics: GameStatistics[] = playerStatisticsStore.getGameStatistics();

    const message = `Game Statistics:
Goals:
${AnnouncementService.buildTopScorersMessage(lastGameStatistics)}
--------
Assists:
${AnnouncementService.buildTopAssistsMessage(lastGameStatistics)}
---------------
Shots on Target:
${AnnouncementService.buildTopShotsOnTargetMessage(lastGameStatistics)} 
---------------
Saves:
${AnnouncementService.buildTopSavesMessage(lastGameStatistics)} `;
    room.sendAnnouncement(message);
  }

  public announceWeeklyScores(): void {
    const weeklyGameStatistics: GameStatistics[] = playerStatisticsStore.getWeeklyGameStatistics();

    const message = `Weekly Statistics:
Goals:
${AnnouncementService.buildTopScorersMessage(weeklyGameStatistics)}
--------
Assists:
${AnnouncementService.buildTopAssistsMessage(weeklyGameStatistics)}
---------------
Shots on Target:
${AnnouncementService.buildTopShotsOnTargetMessage(weeklyGameStatistics)}
---------------
Saves:
${AnnouncementService.buildTopSavesMessage(weeklyGameStatistics)}  `;
    room.sendAnnouncement(message);
  }

  private static buildTopScorersMessage(
    gameStatistics: GameStatistics[]
  ): string {
    return gameStatistics
      .filter(
        (gameStatistic: GameStatistics) =>
          gameStatistic.statistics.goalsScored !== 0
      )
      .sort((a, b) => b.statistics.goalsScored - a.statistics.goalsScored)
      .map(
        (gameStatistic: GameStatistics) =>
          `${gameStatistic.playerName}: ${gameStatistic.statistics.goalsScored}`
      )
      .join("\n");
  }
  private static buildTopAssistsMessage(
    gameStatistics: GameStatistics[]
  ): string {
    return gameStatistics
      .filter(
        (gameStatistic: GameStatistics) =>
          gameStatistic.statistics.assists !== 0
      )
      .sort((a, b) => b.statistics.assists - a.statistics.assists)
      .map(
        (gameStatistic: GameStatistics) =>
          `${gameStatistic.playerName}: ${gameStatistic.statistics.assists}`
      )
      .join("\n");
  }

  private static buildTopShotsOnTargetMessage(
    gameStatistics: GameStatistics[]
  ): string {
    return gameStatistics
      .filter(
        (gameStatistic: GameStatistics) =>
          gameStatistic.statistics.shotsOnTarget !== 0
      )
      .sort((a, b) => b.statistics.shotsOnTarget - a.statistics.shotsOnTarget)
      .map(
        (gameStatistic: GameStatistics) =>
          `${gameStatistic.playerName}: ${gameStatistic.statistics.shotsOnTarget}\n`
      )
      .join("");
  }

  private static buildTopSavesMessage(
    gameStatistics: GameStatistics[]
  ): string {
    return gameStatistics
      .filter(
        (gameStatistic: GameStatistics) =>
          gameStatistic.statistics.shotsSaved !== 0
      )
      .sort((a, b) => b.statistics.shotsSaved - a.statistics.shotsSaved)
      .map(
        (gameStatistic: GameStatistics) =>
          `${gameStatistic.playerName}: ${gameStatistic.statistics.shotsSaved}\n`
      )
      .join("");
  }
}

export const announcementService = new AnnouncementService();

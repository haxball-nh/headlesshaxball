import { BallKickedBy, BallTouchedBy } from "../models/Ball";
import { Player } from "../models/haxball/Player";

export class BallUtility {
  public static getLastBallContact(
    ballKickedBy: BallKickedBy,
    ballTouchedBy: BallTouchedBy
  ): Player | null {
    // if kicked and touched return last contact
    if (ballKickedBy.player !== null && ballTouchedBy.player !== null) {
      return ballKickedBy.time > ballTouchedBy.time
        ? ballKickedBy.player
        : ballTouchedBy.player;
    }

    if (ballKickedBy.player !== null) {
      return ballKickedBy.player;
    }

    if (ballTouchedBy.player !== null) {
      return ballTouchedBy.player;
    }

    // if no last touch return null
    return null;
  }
}

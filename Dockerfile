FROM node:alpine

RUN apk update && apk add --no-cache nmap && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
    apk update && \
    apk add --no-cache \
      chromium \
      harfbuzz \
      "freetype>2.8" \
      ttf-freefont \
      nss

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true

RUN mkdir -p /usr/haxball
WORKDIR /usr/haxball

COPY . .
RUN npm ci && npm run bundle && rm -rf node_modules && npm i --only=prod

EXPOSE 80:80
EXPOSE 443:443

ENV ROOM_NAME="REPLACE_ME"
ENV TOKEN="REPLACE_ME"
ENV PASSWORD="REPLACE_ME"
ENV SAS_TOKEN="REPLACE_ME"
ENV TABLE_URL="REPLACE_ME"

CMD [ "npm", "start" ]
